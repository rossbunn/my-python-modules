import os
import pandas as pd
import numpy as np

def sitedata(citydatafile = os.path.join("/Users/bunnr/1_DATA/sounding_bom_cities/info", "capital_city_data.csv")):
    """Function gets all Bureau Capital City data from existing file, 
        and returns arrays of station names, ids lat lons etc etc"""
    assert type(citydatafile)==str, "filename is not a string"
    assert os.path.isfile(citydatafile) ==True, "file path or file name does not exist"
    
    print "getting site data from"+citydatafile

    # converters pads BoM station number with 2 leading zeros, 
    # needed for matching with present weather data file names
    dfstations = pd.read_csv(citydatafile, 
                              converters={'BoM Station Number': lambda x: str(x).zfill(6)},
                              index_col=False)
    # *********************************************
    # define other station data arrays
    # *********************************************
    statname = np.zeros(len(dfstations), dtype=object)*np.nan
    wmoid = np.zeros(len(dfstations), dtype=object)*np.nan
    bomid = np.zeros(len(dfstations), dtype=int)*np.nan
    lat = np.zeros(len(dfstations), dtype=float)*np.nan
    lon = np.zeros(len(dfstations), dtype=float)*np.nan

    for j in range(len(dfstations)):
        statname[j] = str(dfstations.loc[j,"Station Name"]).rstrip()
        wmoid[j] = str(dfstations.loc[j,"WMO Number"]).strip()
        bomid[j] = int(str(dfstations.loc[j,"BoM Station Number"]).strip())
        lat[j] =  float(str(dfstations.loc[j,"Lat"]).strip())
        lon[j] = float(str(dfstations.loc[j,"Lon"]).strip()) 
        
    return statname, wmoid, bomid, lat, lon