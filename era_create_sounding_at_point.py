import numpy as np
from scipy import interpolate
from scipy.interpolate import interp1d
from nco import Nco  # for importing netCDF variables
import pandas as pd       # for reading csv files and  dataframes
nco = Nco()          # for calling nco instead of Nco
import glob
import time
import os.path
import os


# *********************************************
# input data - Giles
# *********************************************
stationid = "94461"
bomsitelat = -25.03
bomsitelon = 128.28
hour = "00"
#hour = "12"

def 

#Era Int input path
erainpath = "/Users/bunnr/1_DATA/eraint_reanalysis/nc/upper/"
outpath = "/Users/bunnr/2_OUTPUT/stable_layer/era_bomsounding_comparison/txt/"+stationid+"/"+hour+"/"

#TESTING
#bomfnames = glob.glob("/Users/bunnr/2_OUTPUT/stable_layer/era_bomsounding_comparison/testing/sounding/sounding*")
#erainpath = "/Users/bunnr/2_OUTPUT/stable_layer/era_bomsounding_comparison/testing/era/"
#outpath = "/Users/bunnr/2_OUTPUT/stable_layer/era_bomsounding_comparison/testing/"

# getting era in levels
ifile = erainpath + "T_RH_SH_20130101_00.nc"                                                          
Pera = np.array(nco.ncks(input=ifile, returnArray="lev")) #extract Era Int Lev points over restricted domain
Pera = Pera.astype(np.float)
Pera[:] = [x / 100 for x in Pera] # convert from Pa to hPa

# create empty Era Interim arrays for later filling
tempera = np.zeros(len(Pera), dtype = float)
tdera = np.zeros(len(Pera), dtype = float)
rhera = np.zeros(len(Pera), dtype = float)
# thetaeera = np.zeros(len(Pera), dtype = float)
# creating arrays  for later filling (mix of temp and missing data)
# thetaebom =  np.zeros(len(Pera))* np.nan 
# creating tdiff, tddiff, arrays  for later filling
# tdiff = np.zeros(len(Pera), dtype=float)* np.nan 
# tddiff = np.zeros(len(Pera), dtype=float)* np.nan 

# *********************************************
#get Era Int Surrounding latons for bom site
# *********************************************
# get the Era Int lon variables over the restricted domain - using the first file from teh Era Int dataset                                                          
xarr = np.array(nco.ncks(input=ifile, returnArray="lon")) #extract Era Int Lat points over restricted domain
xarr = xarr.astype(np.float)
xdiff = xarr[1]-xarr[0]
yarr = np.array(nco.ncks(input=ifile, returnArray="lat")) #extract Era Int Lon points over restricted domain
yarr = yarr.astype(np.float)
ydiff = abs(yarr[1]-yarr[0])

# returns array index where conditions are met, as well as datatype etc
x0 = np.where((xarr>(bomsitelon-xdiff))&(xarr<bomsitelon))
x1 = np.where((xarr<(bomsitelon+xdiff))&(xarr>bomsitelon))
y0 = np.where((yarr>(bomsitelat-ydiff))&(yarr<bomsitelat))
y1 = np.where((yarr<(bomsitelat+ydiff))&(yarr>bomsitelat))

# get lat and lon values based on found indices
xsurrpoints = np.array([float(xarr[x0]),float(xarr[x1])])
ysurrpoints = np.array([float(yarr[y0]),float(yarr[y1])])

#get only the useful parts of the np.where output (array([72]),), ie array index numbers
x0 = str(x0)
x0 = int(x0[8:10])
x1 = str(x1)
x1 = int(x1[8:10])
y0 = str(y0)
y0 = int(y0[8:10])
y1 = str(y1)
y1 = int(y1[8:10])
