"""Takes list of datetime stamp OBJECTS, and converts to any other format. 
    Default format is YYYYMMDD_HH.
    return dtstampnew. 
    
    Use by typing the following in your script
    
    import convertdatestamp
    newdates = map(convertdatestamp.newdatetimes, listdtstampstoconvert)

    """

def newdatetimes(stamplist, fmt = "%Y%m%d_%H"):
    fmtstring = "{:"+fmt+"}"
    newstamplist = fmtstring.format(stamplist)
    return newstamplist

