"""Module maps datetime stamps to the nearest 4 times in one day.        
   Useful for mapping observed sounding datetimes, to nearest Era Interim datetime.

   mapto_eratimes(sourcestamps, targethours, dhr=4,checktimes=True):
       Function takes existing datetime stamp objects and 4 target hours, 
       and if datetime stamp hour is:
       (a) NOT equal to the targethour
       (b) falls within 4 hours of a target hour
       ... then the datetime stamp is changed to that closest target hour.
       Returns dataframe of new datetimes.
       
    checkmappedtimes(newdttimes, sourcestamps, dhr):
         Checking newdttimes against old - see if mapping occurred correctly. 
         set dhr to "0" for unchanged datetimes, 
         set dhr >4, ie"5" for erroneous conversions, 
         set dhr between "1" and "4" correct conversions
         Prints old, new datetimes and differences to screen"""
         
import datetime as dt
import numpy as np
import pandas as pd


def mapto_eratimes(sourcestamps, targethours, dhr=4,checktimes=True):
    """Function takes existing datetime stamp objects and 4 target hours, 
       and if datetime stamp hour is:
       (a) NOT equal to the targethour
       (b) falls within 4 hours of a target hour
       ... then the datetime stamp is changed to that closest target hour.
       Returns dataframe of new datetimes.
       Useful for mapping observed sounding datetimes, to nearest Era Interim datetime."""
#     assert sourcestamps are datetime stamp objects!!!
    assert len(targethours)==4, "Must have 4 target hours to map to."
    assert isinstance(targethours[0], int), "targethours[0] index is not an int. Are you passing a list element?"
    assert isinstance(targethours[1], int), "targethours[1] index is not an int. Are you passing a list element?"    
    assert isinstance(targethours[2], int), "targethours[2] index is not an int. Are you passing a list element?"
    assert isinstance(targethours[3], int), "targethours[3] index is not an int. Are you passing a list element?"
    
    # create target datetime stamps from target hours
    targetdt = [((dt.datetime.combine(dt.date(1,1,1),dt.time(int(targethours[0]),0,0))).time()),
                ((dt.datetime.combine(dt.date(1,1,1),dt.time(int(targethours[1]),0,0))).time()),
                ((dt.datetime.combine(dt.date(1,1,1),dt.time(int(targethours[2]),0,0))).time()),
                ((dt.datetime.combine(dt.date(1,1,1),dt.time(int(targethours[3]),0,0))).time())]
    # define time deltas
    deltahr = dt.timedelta(hours = dhr)
    deltaday = dt.timedelta(days=1)

    # define arrays
    index = sourcestamps.index
    columns =["datestamp"]
    newdttimes = pd.DataFrame(sourcestamps, index=index, columns=columns )
    
    leni = len(sourcestamps)
    for i in sourcestamps.index:
        condition = (sourcestamps.dt.hour[i]==20)|(sourcestamps.dt.hour[i]==21)|(sourcestamps.dt.hour[i]==22)|(sourcestamps.dt.hour[i]==23)
        for j in range(len(targethours)):     
            if condition:
                eradatetime = dt.datetime.combine(sourcestamps[i].date() + deltaday,targetdt[j])
                dtrange = eradatetime- deltahr
                revx = eradatetime- sourcestamps[i]
                if checktimes:
                    print sourcestamps.dt.hour[i]
                    print dtrange, sourcestamps[i], eradatetime, revx
                if (sourcestamps[i] >= dtrange)&(sourcestamps[i] < eradatetime)&(revx<=dt.timedelta(hours=dhr)):
                    newdttimes.set_value(i,"datestamp",eradatetime)
                    if checktimes:
                        print "revx datetime diff", revx, revx<=dt.timedelta(hours=dhr)
            elif (sourcestamps.dt.hour[i]!=targethours[j]):
                eradatetime = dt.datetime.combine(sourcestamps[i].date(),targetdt[j]) 
                dtrange = eradatetime- deltahr
    #             x = stamptest[i] - eradatetime
                x = eradatetime- sourcestamps[i]
                if checktimes:
                    print sourcestamps.dt.hour[i]
                    print dtrange, sourcestamps[i], eradatetime, x
                if (sourcestamps[i] >= dtrange)&(sourcestamps[i] < eradatetime)&(x<=dt.timedelta(hours=dhr)):
                    newdttimes.set_value(i,"datestamp",eradatetime)
                    if checktimes:
                        print "x datetime diff", x, x<=dt.timedelta(hours=dhr)
    return newdttimes

def checkmappedtimes(newdttimes, sourcestamps, dhr):
    """Checking newdttimes against old - see if mapping occurred correctly. 
         set dhr to "0" for unchanged datetimes, 
         set dhr >4, ie"5" for erroneous conversions, 
         set dhr between "1" and "4" correct conversions"""
    assert len(sourcestamps) == len(newdttimes), "new and old dtstamp arrays must be same length"
    
    ix =0
    for i in sourcestamps.index:
        dtdiff = newdttimes.datestamp[i]-sourcestamps[i]
        if dtdiff==dt.timedelta(hours=dhr):
            ix=ix+1
            print newdttimes.datestamp[i], sourcestamps[i], dtdiff
    if ix==0:
        print "no converted datetime stamps have new-old differences equal to "+str(dhr)