"""Module has a range of functions related to reading in sounding data from many sources, 
    creating numpy arrays from netCDF data, horizontal interpolation and dewpoint trace estimation.
    
    readtracedata(fpathfname, uwyoming = False):
        reads in trace data from filepath and file name.
        return dftrace
    
    bom(fpathfname,sndtime):
        With given BoM sounding location ID and year, BoM sounding data reads in one year's worth of data at a bom site 
        with fname UA01D_Data_["BoM Station ID"]_[YYYY]_43103838703834.txt. Takes sounding fname, and particular date and
        time to get the sounding. NOTE sounding date and time must be a string of format "%Y-%m-%d %H:%M:%S".
        return pres, temp, dwpt, hght

    uniwyoming(fpathfname,sndtime):
        Takes in file path and name of Uni Wyoming sounding data at one site for a whole year.
        Sounding data is at "/Users/bunnr/1_DATA/sounding_uwyoming/"+str(dfsitedata['WMO ID'][ii])+"/YYYY.out".
        return pres, temp, dwpt, hght
        
    uwyomingweb(fpathfname, returnall = False):
        NEEDS TESTING!!!
        Takes web downloaded uwyoming trace data and returns pres, temp, dwpt, rh, geopot height, 
        wind speed, wind direction.
        if returnall = True
            return pres, temp, dwpt, hght, relh, mixr, drct, sknt, thta, thte, thtv 
        if returnall = False
            return pres, temp, dwpt, hght, drct, sknt
            
    geteradata(fpathfname, pvarname="lev", tvarname="var130", rhvarname="var157", ghvarname="var129", lateraname="lat", loneraname="lon" ):
        From eraint .nc file, load temperature, rel hum, geopotential height variables into numpy arrays.
        return  pres, tgrid, rhgrid, ghgrid, latera, lonera
        
    getsurr(sitelat, sitelon, latera, lonera):
        Function takes arrays of latitude and longitude, and finds all surrounding lats and lons to a given point. 
        Returns indices of surrounding lats and lons.
        return x0, x1, y0, y1
    
    interpxy(sitelat, sitelon,x0, x1, y0, y1, pres, tgrid, rhgrid, latera, lonera, ghgrid=False):
        Takes site lat and lon, grid point indices surrounding the site, and gridded data, 
        and linearly interpolates to the site at every vertical level.
        return pres, temp, rh, gh
        
    tdtrace(pres, temp, rh):
        Takes list of pres, temp, rh, and calculates dewpoint temperature at every vertical 
        level.
        NOTE - RH to Td calculation approximation valid for -40 degC < T < 50 degC, 1% < RH < 100%
        Constants for Td calculation are from  Alduchov and Eskridge (1996)
        return dwpt
        
        """

import os
import sys
import pandas as pd
import datetime as dt
import numpy as np
import dewpoint
from scipy import interpolate
from nco import Nco  # for importing netCDF variables
nco = Nco()          # for calling nco instead of Nco

def readtracedata(fpathfname, uwyoming = False):
    """reads in trace data from filepath and file name."""
    assert os.path.isfile(fpathfname), "file or path doesn't exist"
    assert isinstance(fpathfname, basestring), "file path and file name must be a string"
    if uwyoming:
        # create large dataframe of all sounding data at a site for 1 year
        dftrace = pd.read_csv(str(fpathfname), header=None, skip_blank_lines=False, keep_default_na=False, na_values=[''])
        dftrace.columns = ["col1"]
    else:
        dftrace = pd.read_csv(str(fpathfname))
    
#     if fillnan==True:
#         print "filling nan with -9999"
#         dftrace = dftrace.replace(np.nan,-9999, regex=True)

    return dftrace
    
def bom(fpathfname,sndtime,fillnan,checksnddata=False, wind=False):
    """With given BoM sounding location ID and year, BoM sounding data reads in one year's worth of data at a bom site 
       with fname UA01D_Data_["BoM Station ID"]_[YYYY]_43103838703834.txt. Takes sounding fname, and particular date and
       time to get the sounding. NOTE sounding date and time must be a string of format "%Y-%m-%d %H:%M:%S".
        
       fillnan = False  - doesn't fill missing, keeps as NaN ...   needed for SkewT
       fillnan = True   - fills missing values with -9999 ... needed for SHARPpy 
       """

#     assert hasattr(sndtimes, '__len__') and (not isinstance(sitelat, str)), "sitelat is not an array"    
#     assert not isinstance(sndtimes, basestring), "sndtimes is not an array" 
    assert os.path.isfile(fpathfname), "file or path doesn't exist"
    assert isinstance(fpathfname, basestring), "file path and file name must be a string"

    # *********************************************
    # Get trace data
    # *********************************************
#         dftrace = pd.read_csv(str(bomtrace[jj]), 
#                               parse_dates={'datestamp':['Day/Month/Year in DD/MM/YYYY format',
#                                                         'Hour24:Minutes in HH24:MI format in UTC - Coordinated Universal Time']})

    # pandas parse_dates is not working with this datetime stamp
    # have to do it separately, apply to df
#     dftrace = pd.read_csv(str(fpathfname))
    dftrace = readtracedata(fpathfname,uwyoming = False) 
    dftrace['dt'] = dftrace.apply(lambda row: dt.datetime.strptime(row['Day/Month/Year in DD/MM/YYYY format']+ row['Hour24:Minutes in HH24:MI format in UTC - Coordinated Universal Time'], '%d/%m/%Y%H:%M'), axis=1)
    dftrace.insert(0,'datestamp', dftrace['dt'] ) # insert new column at position 0
    dftrace.drop(["dt"], axis=1, inplace=True)    # drop unwanted column
    if checksnddata:
       print "************DFTRACE - START OF YEAR****************"
       print dftrace.ix[0:5,0:9]
       print "************DFTRACE - START OF YEAR****************"
    
    dftrace = dftrace.applymap(lambda x: np.nan if isinstance(x, basestring) and x.isspace() else x)   # replace whitespace with NaN
    if fillnan ==False:
        dftrace = dftrace.dropna(subset=["Air temperature in Degrees C"])  # drop all rows with missing temp
        dftrace = dftrace.dropna(subset=["Geopotential height in gpm to nearest 0.1m"])  # drop all rows with missing height - SKEWT cant handle NaN hght

    # convert sounding date time string to datetime stamp
    sndtimestamp = dt.datetime.strptime(sndtime,"%Y-%m-%d %H:%M:%S")
    # chop years worth traces to dfts.date
    dfsnd = dftrace[dftrace["datestamp"] == sndtimestamp]
#     print "sndtime", sndtime, "sndtimestamp", sndtimestamp  
    
    pres = np.array(dfsnd["Pressure in hPa"],dtype=float)
    temp = np.array(dfsnd["Air temperature in Degrees C"],dtype=float)
    dwpt = np.array(dfsnd["Dew point temperature in Degrees C"],dtype=float)
    hght = np.array(dfsnd["Geopotential height in gpm to nearest 0.1m"],dtype=float)

    
    if checksnddata:
        print dfsnd.iloc[0:5]
        print pres[0:5],temp[0:5],dwpt[0:5],hght[0:5]

    if (fillnan==True) & (len(pres)*len(temp)*len(dwpt)*len(hght)!=0):
        pres = pd.DataFrame(pres)
        pres = pres.applymap(lambda x: -9999 if  x<=-100.0 else x)
        pres = pres.applymap(lambda x: -9999 if (isinstance(x, basestring) and x.isspace()) else x)   # replace whitespace and NaN with -9999
        pres = pres.where((pd.notnull(pres)), int(-9999))   # replace NaN with -9999
        pres = (pres[pres.columns[0]]).tolist()
        temp = pd.DataFrame(temp)
        temp = temp.applymap(lambda x: -9999 if  x<=-100.0 else x)
        temp = temp.applymap(lambda x: -9999 if (isinstance(x, basestring) and x.isspace()) else x)   # replace whitespace and NaN with -9999
        temp = temp.where((pd.notnull(temp)), int(-9999))   # replace NaN with -9999
        temp = (temp[temp.columns[0]]).tolist()
        dwpt = pd.DataFrame(dwpt)
        dwpt = dwpt.applymap(lambda x: -9999 if  x<=-100.0 else x)
        dwpt = dwpt.applymap(lambda x: -9999 if (isinstance(x, basestring) and x.isspace()) else x)   # replace whitespace and NaN with -9999
        dwpt = dwpt.where((pd.notnull(dwpt)), int(-9999))   # replace NaN with -9999
        dwpt = (dwpt[dwpt.columns[0]]).tolist()
        hght = pd.DataFrame(hght)
        hght = hght.applymap(lambda x: -9999 if  x<=-100.0 else x)
        hght = hght.applymap(lambda x: -9999 if (isinstance(x, basestring) and x.isspace()) else x)   # replace whitespace and NaN with -9999
        hght = hght.where((pd.notnull(hght)), int(-9999))   # replace NaN with -9999
        hght = (hght[hght.columns[0]]).tolist()
        
    elif (fillnan==False) & (len(pres)*len(temp)*len(dwpt)*len(hght)!=0):
        pres = pd.DataFrame(pres)
        pres = pres.applymap(lambda x: np.nan if  x<=-100.0 else x)
        pres = pres.applymap(lambda x: np.nan if (isinstance(x, basestring) and x.isspace()) else x)   # replace whitespace and NaN with -9999
        pres = (pres[pres.columns[0]]).tolist()
        temp = pd.DataFrame(temp)
        temp = temp.applymap(lambda x: np.nan if  x<=-100.0 else x)
        temp = temp.applymap(lambda x: np.nan if (isinstance(x, basestring) and x.isspace()) else x)   # replace whitespace and NaN with -9999
        temp = (temp[temp.columns[0]]).tolist()
        dwpt = pd.DataFrame(dwpt)
        dwpt = dwpt.applymap(lambda x: np.nan if  x<=-100.0 else x)
        dwpt = dwpt.applymap(lambda x: np.nan if (isinstance(x, basestring) and x.isspace()) else x)   # replace whitespace and NaN with -9999
        dwpt = (dwpt[dwpt.columns[0]]).tolist()
        hght = pd.DataFrame(hght)
        hght = hght.applymap(lambda x: np.nan if  x<=-100.0 else x)
        hght = hght.applymap(lambda x: np.nan if (isinstance(x, basestring) and x.isspace()) else x)   # replace whitespace and NaN with -9999
        hght = (hght[hght.columns[0]]).tolist()
        
    elif (len(pres)*len(temp)*len(dwpt)*len(hght)==0):
        pres=temp=dwpt=hght=[]
           
    if wind==True:
        wdir = np.array(dfsnd["Wind speed measured in knots"],dtype=float)
        wspd = np.array(dfsnd["Wind direction measured in degrees"],dtype=float)
        wdir = pd.DataFrame(wdir)
        wdir = wdir.applymap(lambda x: np.nan if  x<=-100.0 else x)
        wdir = wdir.applymap(lambda x: np.nan if (isinstance(x, basestring) and x.isspace()) else x)   # replace whitespace and NaN with -9999
        wdir = (wdir[wdir.columns[0]]).tolist()
        wspd = pd.DataFrame(wspd)
        wspd = wspd.applymap(lambda x: np.nan if  x<=-100.0 else x)
        wspd = wspd.applymap(lambda x: np.nan if (isinstance(x, basestring) and x.isspace()) else x)   # replace whitespace and NaN with -9999
        wspd = (wspd[wspd.columns[0]]).tolist() 
        return pres, temp, dwpt, hght, wdir, wspd
    else:
        return pres, temp, dwpt, hght


def uniwyoming(fpathfname,sndtime):
    """Takes in file path and name of Uni Wyoming sounding data at one site for a whole year.
        Sounding data is at "/Users/bunnr/1_DATA/sounding_uwyoming/"+str(dfsitedata['WMO ID'][ii])+"/YYYY.out"."""
    assert os.path.isfile(fpathfname), "file or path doesn't exist"
    assert isinstance(fpathfname, basestring), "file path and file name must be a string"
        
    #temp file to delimit bom sounding files
    tmpfile = os.path.join(os.getcwd(),"ftmp1.txt")
    f = open(tmpfile, 'w')
    f.close()
    
    # convert sounding date time string to datetime stamp
    sndtimestamp = dt.datetime.strptime(sndtime,"%Y-%m-%d %H:%M:%S")
    
    sndyr = fpathfname[-8:-4]
    # create index array of row locations marking the start of new sounding data
    # assume there are at most 2 soundings per day + a bit for extra, length = 365*3 
    ix = np.zeros(365*3, dtype=int)*np.nan

    # create large dataframe of all sounding data at a site for 1 year
#   dftrace = pd.read_csv(str(fpathfname), header=None, skip_blank_lines=False, keep_default_na=False, na_values=[''])
#   dftrace.columns = ["col1"]
    dftrace = readtracedata(fpathfname, uwyoming = True)

    num0 = 0 
    num = 0
    
    for j in range(len(dftrace)):
        if "Observations at" in str(dftrace.col1[j]):
            ix[num] = j
            num=num0 + 1   # count for number of matched "Observations at" lines
            num0 = num
    # reset indices for "Observations at" lines
    num0 = 0 
    num = 0
    # drop nan in ix array
    ix = np.array((pd.Series(ix).dropna()), dtype=int)

    # for each sounding in the whole years worth in dftrace
    for pp in range(len(ix)):
        a = str(dftrace.col1[ix[pp]])
        datestr = a[-15:len(a)]
        # *********************************************
        # create datetime stamp from datestr
        # ********************************************* 
        yr = datestr[-4:len(datestr)]
        mt = datestr[-8:-5]
        dy = datestr[-11:-9]
        hr = datestr[-15:-13]
        format = "%Y%b%d%H"
        snddateuwyom = pd.to_datetime(str(yr).zfill(2)+str(mt)+\
                                 str(dy).zfill(2)+str(hr).zfill(2), format=format)


#             dftsmonth = str(dfts["datestamp"].dt.month[jj]).zfill(2)
        if snddateuwyom != sndtimestamp:
            del snddateuwyom
            continue

        print "matching dates!!! ", snddateuwyom, sndtimestamp

#             if datetime != '12Z 06 Feb 2008': continue    # debugging!!!
        if pp < (len(ix)-1):
            dftmp = dftrace[ix[pp] : ix[pp+1]]
        else:
            dftmp = dftrace[ix[pp] : len(dftrace)]
            
    # create new df with datestring and blank line, then append existing df to it
    # exact formatting is needed for the SkewT module       
    #   del top line of dftmp   
    dftmp = dftmp.drop(ix[pp])
    dftmp = pd.DataFrame(np.array([datestr, ' ']), columns=['col1']).append(dftmp, ignore_index=True)
    # write temperary file for later reading - space delimited EXACT format needed for skewt module
    dftmp.to_csv(tmpfile, header=None, index=False)    
    S=SkewT.Sounding(tmpfile)
    os.remove(tmpfile)
    temp = S.soundingdata['temp']
    dwpt = S.soundingdata['dwpt']
    pres = S.soundingdata['pres']
    hght = S.soundingdata['hght']
    
    return pres, temp, dwpt, hght

def uwyomingweb(fpathfname, returnall = False):
    """NEEDS TESTING!!!
        Takes web downloaded uwyoming trace data and returns pres, temp, dwpt, rh, geopot height, 
        wind speed, wind direction."""
    assert os.path.isfile(fpathfname), "file or path doesn't exist"
    assert isinstance(fpathfname, basestring), "file path and file name must be a string"
    
    #temp files to delimit bom sounding files
    tmpfile1 = os.path.join(os.getcwd(),"ftmp1.txt")
    f = open(tmpfile1, 'w')
    f.close()
    tmpfile2 = os.path.join(os.getcwd(),"ftmp2.txt")
    f = open(tmpfile2, 'w')
    f.close()
    
    
    # *********************************************
    # getting Bom sounding T and Td data
    # *********************************************
    df = pd.read_csv(fpathfname)
    df.columns = ["col1"]
    strdate = str(df.col1[3])
    df.col1.map(len).max()                                           # get max row length
    df = df[(df.col1.map(len) == df.col1.map(len).max())]            # retain only rows with max length
    df = df.set_index(np.array(range(len(df))))                      # relist rows 0...N
    header = str(df.col1[1]).split()                                 # get header
    df = df.drop(df.index[[0,1,2,3]])                                # get rid of header
    df = df.set_index(np.array(range(len(df))))                      # relist rows 0...N
    #df = pd.DataFrame(df.col1.str.split().tolist(), columns=header)  # split col1 string, & add strings to df
    # can't split header at end of word retaining leading whitespace.... this almost works 
    # re.split("\s(?!\w{1})", str(df.col1[1])), but gives " ", " ", " PRES", NOT "   PRES"
    # getting number of columns
    ncols = len(str(df.col1[1]).split())                             # assuming even width (which they are in this case)
    colwidth = int(df.col1.map(len).max()/ncols)                     # get colwidth
    # write temperary file for later reading, with commas inserted at end of each column width
    df.to_csv(tmpfile1, header=None, index=False)  
    fin = open(tmpfile1, 'r')
    fout = open(tmpfile2, 'w')
    for line in fin:
        newline = ','.join([line[x:x+colwidth] for x in range(0, len(line),colwidth)])
        fout.write(newline)
    fin.close()
    fout.close()
    df = pd.read_csv(tmpfile2, ",", names = header, index_col=False)    # reading in the tmpfile2 file now comma delimitted
    df = df.applymap(lambda x: np.nan if isinstance(x, basestring) and x.isspace() else x)   # replace whitespace with NaN

    if returnall:
    #   returning PRES   HGHT   TEMP   DWPT   RELH   MIXR   DRCT   SKNT   THTA   THTE   THTV
        pres = np.array(df.PRES.values, dtype = float)
        temp = np.array(df.TEMP.values, dtype = float)
        hght = np.array(df.HGHT.values, dtype = float)
        dwpt = np.array(df.DWPT.values, dtype = float)
        relh = np.array(df.RELH.values, dtype = float)
        mixr = np.array(df.MIXR.values, dtype = float)
        drct = np.array(df.DRCT.values, dtype = float)
        sknt = np.array(df.SKNT.values, dtype = float)
        thta = np.array(df.THTA.values, dtype = float)
        thte = np.array(df.THTE.values, dtype = float)
        thtv = np.array(df.THTV.values, dtype = float)
        
        return pres, temp, dwpt, hght, relh, mixr, drct, sknt, thta, thte, thtv
    else:
        #   returning PRES   HGHT   TEMP   DWPT   DRCT   SKNT
        pres = np.array(df.PRES.values, dtype = float)
        temp = np.array(df.TEMP.values, dtype = float)
        hght = np.array(df.HGHT.values, dtype = float)
        dwpt = np.array(df.DWPT.values, dtype = float)
        drct = np.array(df.DRCT.values, dtype = float)
        sknt = np.array(df.SKNT.values, dtype = float)
        
        return pres, temp, dwpt, hght, drct, sknt


        T = np.array(df.TEMP.values, dtype = float)
        TD = np.array(df.DWPT.values, dtype = float)
        del df

        

def geteradata(fpathfname, pvar="lev", tvar="var130", rhvar="var157", gpotvar="var129", latera="lat", lonera="lon" ):
    """From eraint .nc file, load temperature, rel hum, geopotential height variables into numpy arrays.
        """
    assert os.path.isfile(fpathfname), "file or path doesn't exist"
    assert isinstance(fpathfname, basestring), "file path and file name must be a string"

    pera = np.array(nco.ncks(input=fpathfname, returnArray=pvar)) #extract Era Int Lev points over restricted domain
    pera = pera.astype(np.float)
    pera[:] = [x / 100 for x in pera] # convert from Pa to hPa

    tgrid = np.array(nco.ncks(input=fpathfname, returnArray=tvar)) #extract Era Int Temp points var[time,lev,lat,lon]
    tgrid = tgrid.astype(np.float)
    rhgrid = np.array(nco.ncks(input=fpathfname, returnArray=rhvar)) #extract Era Int RH points var[time,lev,lat,lon]
    rhgrid = rhgrid.astype(np.float) 
    # must convert geopotential in m^2/s^2 to geopotential height in m, by dividing by gravitational acceleration g=9.8m/s^2
    g =  9.80665 # m/s^2
    ghgrid = np.array(nco.ncks(input=fpathfname, returnArray=gpotvar)) / g 
    ghgrid = ghgrid.astype(np.float) 
    lonera = np.array(nco.ncks(input=fpathfname, returnArray=lonera)) #extract Era Int Lon points over restricted domain
    lonera = lonera.astype(np.float)
    latera = np.array(nco.ncks(input=fpathfname, returnArray=latera)) #extract Era Int Lat points over restricted domain
    latera = latera.astype(np.float)

    return  pera, tgrid, rhgrid, ghgrid, latera, lonera

def getsurr(sitelat, sitelon, latera, lonera):
    """Function takes arrays of latitude and longitude, and finds all surrounding lats and lons to a given point. 
    Returns indices of surrounding lats and lons."""
    assert isinstance(sitelat, float), "sitelat is not a float. Are you passing a list element?"
    assert isinstance(sitelon, float), "sitelon is not a float. Are you passing a list element?"
    
    # get lat and lon differences and assume it is a regular grid
    londiff = lonera[1]-lonera[0]
    latdiff = abs(latera[1]-latera[0])

    # returns array index where conditions are met, as well as datatype etc
    x0 = np.where((lonera > (sitelon - londiff)) & (lonera < sitelon)) # min x
    x1 = np.where((lonera < (sitelon + londiff)) & (lonera > sitelon)) # max x
    y0 = np.where((latera > (sitelat - latdiff)) & (latera < sitelat)) # min y (higher index number)
    y1 = np.where((latera < (sitelat + latdiff)) & (latera > sitelat)) # max y (lower index number)

    # np.where returns tuple (array([72]),), need only array indices, so:
    x0 = x0[0]
    x0 = int(x0)
    x1 = x1[0]
    x1 = int(x1)
    y0 = y0[0]
    y0 = int(y0)
    y1 = y1[0]
    y1 = int(y1)
    
    return x0, x1, y0, y1
    
def interpxy(sitelat, sitelon,x0, x1, y0, y1, pres, tgrid, rhgrid, latera, lonera, ghgrid, checksurrpoints = False):
    """Takes site lat and lon, grid point indices surrounding the site, and gridded data, 
        and linearly interpolates to the site at every vertical level. """
    assert isinstance(sitelat, float), "sitelat is not a float. Are you passing a list element?"
    assert isinstance(sitelon, float), "sitelon is not a float. Are you passing a list element?"
    assert isinstance(x0, int), "x0 index is not an int. Are you passing a list element?"
    assert isinstance(x1, int), "x1 index is not an int. Are you passing a list element?"    
    assert isinstance(y0, int), "y0 index is not an int. Are you passing a list element?"
    assert isinstance(y1, int), "y1 index is not an int. Are you passing a list element?"

    if checksurrpoints == True:
        lenpres = len(pres)
        print "lenpres",lenpres
        print "tgridshape", tgrid.shape
        print "rhgridshape", rhgrid.shape
        print "ghgridshape", ghgrid.shape
        lefttop = str(lonera[x0])+"_"+str(latera[y1])
        righttop = str(lonera[x1])+"_"+str(latera[y1])
        leftbot = str(lonera[x0])+"_"+str(latera[y0])
        rightbot = str(lonera[x1])+"_"+str(latera[y0])
        print x0, x1, y0, y1
        print [lonera[x0], latera[y1]], [lonera[x1], latera[y1]]
        print [lonera[x0], latera[y0]], [lonera[x1], latera[y0]]
        print np.array([lefttop,righttop ,leftbot, rightbot]).reshape(2,2)
        print np.array(["leftop","righttop" ,"leftbot", "rightbot"]).reshape(2,2)
        print sitelon, sitelat
        print "from scipy interpolate2d help: z array to imterp must me of shape:"
        print "If the points lie on a regular grid, `x` can specify the column coordinates and `y` the row coordinates, for example::"
        print "x = [0,1,2];  y = [0,3]; z = [[1,2,3], [4,5,6]]"
        print np.array([ ["leftop","righttop"] ,["leftbot", "rightbot"]])

    
    # create empty arrays for later filling
    temp = np.zeros(len(pres), dtype = float)
    rh = np.zeros(len(pres), dtype = float)
    gh = np.zeros(len(pres), dtype = float)
    if checksurrpoints:
        # create empty arrays for later filling
        temp_lefttop = np.zeros(len(pres), dtype = float)
        rh_lefttop = np.zeros(len(pres), dtype = float)
        gh_lefttop = np.zeros(len(pres), dtype = float)
        
        temp_righttop = np.zeros(len(pres), dtype = float)
        rh_righttop = np.zeros(len(pres), dtype = float)
        gh_righttop = np.zeros(len(pres), dtype = float)
        
        temp_leftbot = np.zeros(len(pres), dtype = float)
        rh_leftbot = np.zeros(len(pres), dtype = float)
        gh_leftbot = np.zeros(len(pres), dtype = float)
        
        temp_rightbot = np.zeros(len(pres), dtype = float)
        rh_rightbot = np.zeros(len(pres), dtype = float)
        gh_rightbot = np.zeros(len(pres), dtype = float)
            
    
    # get lat and lon values based on found indices
    xsurrpoints = np.array([float(lonera[x0]),float(lonera[x1])])
    ysurrpoints = np.array([float(latera[y0]),float(latera[y1])])
    
    # for all Era Int levels, interpolate linearly to BoM site latlon location
    # based on 4 surrounding points era int points (prefiously defined for site YMML)
    lenpres = len(pres)
    for i in range(lenpres):
#         print i, pres[i]
        # create Era Int Temerature array
#         tsurr= np.array([tgrid[0,i,y0,x0],tgrid[0,i,y1,x0],tgrid[0,i,y0,x1],tgrid[0,i,y1,x1]]).reshape(2,2)
        tsurr= np.array([ [tgrid[0,i,y1,x0],tgrid[0,i,y1,x1]] , [tgrid[0,i,y0,x0],tgrid[0,i,y0,x1]] ])
        f1 = interpolate.interp2d(xsurrpoints, ysurrpoints, tsurr, kind='linear')
        temp[i] = f1(sitelon,sitelat) - 273.15

        # create Era Int RH  array
#         rhsurr= np.array([rhgrid[0,i,y0,x0],rhgrid[0,i,y1,x0],rhgrid[0,i,y0,x1],rhgrid[0,i,y1,x1]]).reshape(2,2)
        rhsurr= np.array([ [rhgrid[0,i,y1,x0],rhgrid[0,i,y1,x1]] , [rhgrid[0,i,y0,x0],rhgrid[0,i,y0,x1]] ])
        f2 = interpolate.interp2d(xsurrpoints, ysurrpoints, rhsurr, kind='linear')
        rh[i] = f2(sitelon,sitelat)
        
         # create Era Int geopotential height array
#         ghsurr= np.array([ghgrid[0,i,y0,x0],ghgrid[0,i,y1,x0],ghgrid[0,i,y0,x1],ghgrid[0,i,y1,x1]]).reshape(2,2)
        ghsurr= np.array([ [ghgrid[0,i,y1,x0],ghgrid[0,i,y1,x1]] , [ghgrid[0,i,y0,x0],ghgrid[0,i,y0,x1]] ])
        f3 = interpolate.interp2d(xsurrpoints, ysurrpoints, ghsurr, kind='linear')
        gh[i] = f3(sitelon,sitelat)
    
        if checksurrpoints:
#             print "index i  in interpxy where checksurrpoints=True",  i
            temp_lefttop[i]  = tgrid[0,i,y1,x0]- 273.15
            rh_lefttop[i]   = rhgrid[0,i,y1,x0]
            gh_lefttop[i]   = ghgrid[0,i,y1,x0]
            temp_righttop[i] = tgrid[0,i,y1,x1]- 273.15
            rh_righttop[i]  = rhgrid[0,i,y1,x1]
            gh_righttop[i]  = ghgrid[0,i,y1,x1]
            temp_leftbot[i]  = tgrid[0,i,y0,x0]- 273.15
            rh_leftbot[i]   = rhgrid[0,i,y0,x0]
            gh_leftbot[i]   = ghgrid[0,i,y0,x0]
            temp_rightbot[i] = tgrid[0,i,y0,x1]- 273.15
            rh_rightbot[i]  = rhgrid[0,i,y0,x1]
            gh_rightbot[i]  = ghgrid[0,i,y0,x1]
        
    if checksurrpoints:    
        return pres, temp, rh, gh, temp_lefttop,rh_lefttop,gh_lefttop,temp_righttop,rh_righttop,gh_righttop,temp_leftbot,rh_leftbot,gh_leftbot,temp_rightbot,rh_rightbot,gh_rightbot,lefttop,righttop,leftbot,rightbot
    else:
        return pres, temp, rh, gh

    
def tdtrace(pres, temp, rh):
    """Takes list of pres, temp, rh, and calculates dewpoint temperature at every vertical 
        level.
        
        NOTE - RH to Td calculation approximation valid for -40 degC < T < 50 degC, 1% < RH < 100%
        Constants for Td calculation are from  Alduchov and Eskridge (1996)"""
        
    # empty dwpt array for later filling
    dwpt = np.zeros(len(pres), dtype = float)
    lenpres = len(pres)
    for i in range(lenpres):
        dwpt[i] = dewpoint.acc_dewpoint_approximation(temp[i],rh[i])
        diff  = temp[i] - dwpt[i]
        if diff <=0:
            print "saturated layer TD>T (errors in TD approx)"
            print pres[i], temp[i], dwpt[i], temp[i] - dwpt[i]

            dwpt[i]= temp[i]  - 0.01
            print "making TD just less than than T"
            print pres[i], temp[i], dwpt[i], temp[i] - dwpt[i]
    return dwpt





