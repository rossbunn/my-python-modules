""" Module functions:
    acc_dewpoint_approximation(T,RH):
        RH to Td calculation approximation valid for -40 degC < T < 50 degC, 1% < RH < 100%
        Equation is Equation 8 from http://climate.envsci.rutgers.edu/pdf/LawrenceRHdewpointBAMS.pdf.
        Constants for Td calculation are from  Alduchov and Eskridge (1996) :"""

import numpy as np


def acc_dewpoint_approximation(T,RH):
    """RH to Td calculation approximation valid for -40 degC < T < 50 degC, 1% < RH < 100%
        Constants for Td calculation are from  Alduchov and Eskridge (1996) :"""
    assert T<100, "T must be in degC, Is T in Kelvin?"
    
    a = 17.625
    b = 243.04 # degC 
    g = np.log(RH/100.0)
    
    Td = b*(g + (a*T/(b+T)))/(a - g - (a*T/(b+T)))
    
    return Td 
