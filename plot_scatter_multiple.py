"""Function takes 2 dataframes of equal shape (df colnames must match and have numbers of notnan data).
   Takes also number of rows and cols (rows X cols == len(df1.columns)), plot titles as df1.colnames
    and x and y labels (df1, df2 names). """
import numpy as np
import pandas as pd
import datetime as dt
import time
import matplotlib.pyplot as plt
import sys
import os
import metadata
from sklearn.metrics import mean_absolute_error as mae

tic = time.clock()

def main(dfpltbom_x,dfpltera_y, rsqu, plotpath=os.getcwd(), plttitle=None,title2line=False,
         fname="newplot", nrows=1, ncols=1, savefig=True, color="red", showplot=True):
    assert dfpltbom_x.shape==dfpltera_y.shape, "number of rows and cols in both dataframes MUST be equal"
    assert (dfpltbom_x.columns.all() == dfpltera_y.columns.all())==True, "column names must be equal - check datecol name?"

    plt.figure(figsize=(12,nrows*4))
    if plttitle != None:
        if title2line:
            ypos = 0.9999
        else:
            ypos = 0.94
        plt.suptitle(plttitle,size=16, x= 0.5, y = ypos)

    allcolsbom = dfpltbom_x.columns
    allcolsera = dfpltera_y.columns

    
    for i in range(2,len(allcolsbom)+1):
#     for i in range(1,len(dfpltbom_x.columns)):

        plt.subplot(nrows,ncols,i) 
        
        dftempbom = dfpltbom_x
        dftempera = dfpltera_y
        
        colsbom = pd.DataFrame(allcolsbom,columns = ["0"])
        colsbomkeep = np.array(allcolsbom)[[0,i-1]]
        dftempbom1 = dftempbom[colsbomkeep]

        print "KEEPING THESE"
        print colsbomkeep

        colsera = pd.DataFrame(allcolsera,columns = ["0"])
        colserakeep = np.array(allcolsera)[[0,i-1]]
        dftempera1 = dftempera[colserakeep]

        colnames1 = dftempbom1.columns
        print "COLNAMES BOM",colnames1
        colnames2 = dftempera1.columns
        print "COLNAMES ERA",colnames2
        if (colnames1==colnames2).all()==True:
            colnames = colnames1

        
        # create data for plotting by matching datetimes from bom and era
        data = pd.DataFrame(index=dftempbom1.index,columns=["bom","era"])
        for j in range(len(dftempbom1)):
            for k in range(len(dftempera)):
                if (dftempbom1["datestamp"][j]==dftempera["datestamp"][k]):
#                     print dftempbom1["datestamp"][j], j, float(dftempbom1.iloc[j][str(colnames[1])]), k, float(dftempera1.iloc[k][str(colnames[1])] )
                    data.set_value(j,"bom",dftempbom1.iloc[j][str(colnames[1])] )
                    data.set_value(j,"era",dftempera1.iloc[k][str(colnames[1])] )
        data = data.dropna(axis=0) #drop any missing rows... just in case
        data = data.set_index(np.array(range(len(data))))  # relist rows 0...N
        print data
        scatterplot(data['bom'].values.tolist(),data['era'].values.tolist(),rsqu, xlabel="BoM",ylabel="EraInt",title=str(colnames[1]), color="green")
        print "DONE - "+str(i) +" " +str(colnames[1])
        
    if savefig:
        plt.gcf().savefig(os.path.join(plotpath,fname+".png"), dpi=500)
        print os.path.join(plotpath,fname+".png")
        metadata.main(plotpath,fname)
    
    if showplot==True:
       plt.show()



def scatterplot(df1column_n,df2column_n,rsqu,xlabel="df1name",ylabel="df2name",title="df1_colname", color="blue"):
    """Plots a scatter plot"""
    assert len(df1column_n)>0, "X df data  needs to be a list of int or float"
    assert len(df2column_n)>0, "Y df data  needs to be a list of int or float"

    #maxes for plotting
    max1 = np.max(df1column_n)
    max2 = np.max(df2column_n) 
    dfmax = max([max1,max2])
    min1 = np.min(df1column_n) 
    min2 = np.min(df2column_n) 
    dfmin = min([min1,min2])
    
    # stats
    Rmse = rmse(df2column_n, df1column_n)
    Mbias = mbias(df1column_n, df2column_n)
    Mae = mae(df1column_n, df2column_n)  # (x_true=bom, y_pred=era)
#     xbar = np.mean(df1column_n) # (x_true=bom)
#     ybar = np.mean(df2column_n) # (y_pred=era)
    Rmse=round(Rmse,1)
    Mbias=round(Mbias,1)
    Mae=round(Mae,1)
#     Rmse=int(Rmse)
#     Mbias=int(Mbias)
#     Mae=int(Mae)
#     xbar=round(xbar,1)
#     ybar=round(ybar,1)

    plt.scatter(df1column_n, df2column_n, s=1,  color=color)
    plt.plot([dfmin, (dfmax+2)], [dfmin, (dfmax+2)], 'k-', lw=2)
    plt.title(title)
    plt.xlabel(xlabel, labelpad=-1)
    plt.ylabel(ylabel, labelpad=1)

#     plt.annotate('RMSE= ' + str(Rmse) +' '  +\
#                  'BIAS= ' + str(Mbias)+' '  +\
#                  'MAE= '  + str(Mae)  +'\n' +\
#                  'xbar= ' + str(xbar) +' '  +\
#                  'ybar= ' + str(ybar),
#                  xy=(1, 0), xycoords='axes fraction', fontsize=10,
#                  xytext=(-5, 5), textcoords='offset points',
#                  ha='right', va='bottom')
    plt.annotate('RMSE= ' + str(Rmse) +' '  +\
                 'BIAS= ' + str(Mbias)+' '  +\
                 'MAE= '  + str(Mae),
                 xy=(1, 0), xycoords='axes fraction', fontsize=10,
                 xytext=(-5, 5), textcoords='offset points',
                 ha='right', va='bottom')
    plt.annotate('n = ' + str(len(df1column_n)),
                 xy=(1, 0), xycoords='axes fraction', fontsize=10,
                 xytext=(-5, 5), textcoords='offset points',
                 ha='right', va='top')
    if rsqu==True:  
        # values needed for functions
        xd = df1column_n
        yd = df2column_n
        minxd = min1
        maxxd = max1
        # call function to calculation x and y values for line best fit
        x1 = xl(minxd, maxxd)
        y1 = yl(x1, xd, yd, minxd, maxxd, order=1)
        # call function calculate 
        Rsqr1,p = Rsqr(xd,yd)
        Rsqr1 = str(round(Rsqr1, 2))
        p = str(p)
        #Plot trendline
        plt.plot(x1, y1, c="r", alpha=1, lw=2)
        plt.text(dfmin, (dfmax+1), Rsqr1 + ', ' + p)
        del xd, yd, x1, y1, minxd, maxxd, Rsqr1

#         # call function calculate 
#         Rsqr1 = Rsqr(df1column_n,df2column_n)
#         #Plot least squares reg as label
#         plt.text(dfmin, (dfmax+1),
#                      '$R^2 = %0.2f$' % Rsqr1)
#       del Rsqr1

    
#function to fir line to scattered data
def xl(minxd, maxxd):
    """get x vals of line best fit"""
    xl = np.array([minxd, maxxd])
    return xl

def yl(xl, xd, yd, minxd, maxxd, order=1):
    """get y vals of line best fit"""

    #Calculate trendline
    coeffs = np.polyfit(xd, yd, order)
    intercept = coeffs[-1]
    slope = coeffs[-2]
    if order == 2: power = coeffs[0]
    else: power = 0

    yl = power * xl ** 2 + slope * xl + intercept
    return yl

def Rsqr(xd,yd,order=1):
    ''' Coefficient of determination for single regression
    http://blog.minitab.com/blog/adventures-in-statistics/regression-analysis-how-do-i-interpret-r-squared-and-assess-the-goodness-of-fit'''
    coeffs = np.polyfit(xd, yd, order)
#     coeffs = [1.0,0.0]
    p = np.poly1d(coeffs)
    print str(p)
    
    ybar = np.sum(yd) / len(yd)
    ssreg = np.sum((p(xd) - ybar) ** 2)
    sstot = np.sum((yd - ybar) ** 2)
    Rsqr = ssreg / sstot
    return Rsqr, p

def rmse(predictions, targets):
    '''Root Mean Square Error
        Parameters
    ----------
    predictions, targets : array_like
       The performance measure depends on the difference between these two
       arrays.

    Returns
    -------
    bias : ndarray or float
       bias, or mean difference along given axis.

    Notes
    -----
    If ``predictions`` and ``targets`` have different shapes, then they need to broadcast.
    This uses ``numpy.asanyarray`` to convert the input. Whether this is the
    desired result or not depends on the array subclass.

    '''
    predictions = np.asanyarray(predictions)
    targets = np.asanyarray(targets)
    rmse = np.sqrt(((predictions - targets) ** 2).mean())
    return rmse
    
def mbias(x1, x2):
    '''bias, mean error

    Parameters
    ----------
    x1, x2 : array_like
       The performance measure depends on the difference between these two
       arrays.
    axis : int
       axis along which the summary statistic is calculated

    Returns
    -------
    mbias : ndarray or float
       bias, or mean difference along given axis.

    Notes
    -----
    If ``x1`` and ``x2`` have different shapes, then they need to broadcast.
    This uses ``numpy.asanyarray`` to convert the input. Whether this is the
    desired result or not depends on the array subclass.

    '''
    x1 = np.asanyarray(x1)
    x2 = np.asanyarray(x2)
    mbias = np.mean(x1-x2) 
    return mbias


toc = time.clock()
print toc - tic
