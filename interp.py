"""Module takes a source array, target array, and data, and for all target array points
   between target array points, 1D linear interpolation of data occurs.
   Returns datainterp"""

import pandas as pd
import numpy as np

def within(p, q, r):
    """Return true iff q is between p and r (inclusive). 
       Bounding function for Bom T, Td interpolation."""
    #return p <= q <= r or r <= q <= p
    return r <= q < p

def interpZ(source,target,data,remnan=True):
    """ Function takes a source array, target array, and data, and for all target array points
        between target array points, 1D linear interpolation of data occurs.
        Source level grid for interp/extrap = BoM Pressure
        Target level grid to obtain BoM T, Td values for  = Era Int lev
        Returns:
           datainterp
           length
    """

    from scipy.interpolate import interp1d
    import numpy as np

    source = np.array(source, dtype = float)
    target = np.array(target, dtype = float)
    data = np.array(data, dtype = float)
    
    # find missing= -9999 values and replace with NaN
    data = pd.DataFrame(data)
    data = data.applymap(lambda x: np.nan if x==-9999 else x)   # replace -9999 values with NaN
    data = (data[data.columns[0]]).tolist()

    datainterp = np.empty(len(target),dtype=float)**np.nan

    for i in range(len(source)-1):
        for j in range(len(target)-1):
            if (i <= (len(source)-1)):
                if (within(source[i], target[j], source[i+1])==True):
                    f1 = interp1d([source[i],source[i+1]], [data[i],data[i+1]], kind='linear')
                    datainterp[j] = f1(target[j])
                    del f1
            else:
                if (within(source[i-1], target[j], source[i])==True):
                    f1 = interp1d([source[i-1],source[i]], [data[i-1],data[i]], kind='linear')
                    datainterp[j] = f1(target[j])
                    del f1
    if remnan==True:
       # remove nan values in datainterp
       datainterp =datainterp[~np.isnan(datainterp)]
    
    length = len(datainterp)
    
    return datainterp, length