
def datacheck(name):
    """Checks data for errors in maximum and minimum """
    assert type(name)==str, "filename is not a string"
    try:
       data=np.loadtxt(name, delimiter=',')
       print "file in datacheck exists"
    except RuntimeError:
       print str(name), "file does not exist"
       return
    
#     data = np.loadtxt(name, delimiter=',')
    if (data.max(axis=0)[0] == 0) & (data.max(axis=0)[20] == 20):
        print str(name)+" - suspicious maximum"
    elif data.min(axis=0).sum == 0:
        print str(name)+" - minima adds to zero"
    else:
        print str(name)+" - data seems ok"

def dataplot(name):
    """Reads data in csv format then \n(a) calculates mean, max and min of all columns in data, \n(b) plots mean, max, min of columns """
    assert type(name)==str, "filename is not a string"
    try:
       data=np.loadtxt(name, delimiter=',')
       print "file in dataplot exists"
    except RuntimeError:
       print str(name), "file does not exist"
       return
       
#     data = np.loadtxt(name, delimiter=',')
#     fig = plt.figure(figsize=(10.0, 3.0))
    fig = figure(figsize=(10.0, 3.0))

    axes1 = fig.add_subplot(1, 3, 1)
    axes2 = fig.add_subplot(1, 3, 2)
    axes3 = fig.add_subplot(1, 3, 3)

    axes1.set_ylabel('average')
    axes1.set_xlabel('days')
    axes1.plot(data.mean(axis=0)) # average of all patient data per day

    axes2.set_ylabel('max')
    axes2.set_xlabel('days')
    axes2.plot(data.max(axis=0)) # max of all patient data per day

    axes3.set_ylabel('min')
    axes3.set_xlabel('days')
    axes3.plot(data.min(axis=0)) # min of all patient data per day

    fig.tight_layout()

    plt.show(fig)
