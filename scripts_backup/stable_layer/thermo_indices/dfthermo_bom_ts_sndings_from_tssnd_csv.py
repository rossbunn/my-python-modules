# Creates BoM TS sounding indices AT ERA LEVS by:
# 1. reading existing BoM TS sounding times from old dfthermo
# 2. grabs only location and sounding time data from old dfthermo, and creates a new dfthermo for populating
# 3. gets EraInt levels 
# 4. on matched BoM sounding dates/time files, at each Cap City site:
#  - vertically interpolats pres, temp dwpt, hght to Era Int levels (checking new arrays aren't small)
#  - pass to SkewT, calc indices and populate dataframe dfthermo

import numpy as np
import pandas as pd       # for reading csv files and  dataframes
import glob
import time
import datetime as dt
from skewt import SkewT
from skewt import thermodynamics as thrm
import sys
import os
import thermoindices
import gettrace
import interp
import metadata

#%matplotlib inline

tic = time.clock()

# *********************************************
# paths
# *********************************************

bompath = "/Users/bunnr/1_DATA/sounding_bom_cities/"
testpath = "/Users/bunnr/python/stable_layer/stable_layer_grid_eraint/testing/"
outpath = "/Users/bunnr/2_OUTPUT/stable_layer/thermo_indices/dataframes/"

# *********************************************
# years where gpats data is available
# *********************************************
gpatsyrs = np.array(["2008", "2009", "2010", "2011", "2012", "2013", "2014"], dtype=str)

# # *********************************************
# #  Reading in dates/times per capital city where 
# #  known gpats activity was recorded and soundings exist
# # *********************************************
# read in existing df, chop unwanted data and save
fname = "dfthermo_gpats_bom_notnan_v3.csv"
dftemp = pd.read_csv(os.path.join(outpath,fname), 
                    converters={'BoM Station ID': lambda x: str(x).zfill(6)}, 
                    parse_dates=['datestamp'], index_col = False)
# want to keep all station data in dftemp from 0:6, 
# but drop all indices stuff from 7: 
# (since it will be recalculated) 
colix = range(len(dftemp.columns))[7:]
dftemp.drop(dftemp.columns[colix], 
                 axis=1, inplace=True)

# # SAVING dfthermo Dataframe PER STATION PER Year to file
dftemp.to_csv(outpath+"dftemp.csv", header = "true")

# files
fname = "dftemp.csv"
dfthermo = thermoindices.create(outpath,fname,dtstampname="datestamp")
os.remove(outpath+"dftemp.csv")

# get list of site data only
dfsitedata = dfthermo.drop_duplicates(subset="Station Name", take_last = False)  # remove duplicates
dfsitedata = dfsitedata.set_index(np.array(range(len(dfsitedata))))  # relist from 0... len(df)
statname = dfsitedata["Station Name"]

# *********************************************
#  arrays needed
# *********************************************
strwdth = "|S"+str(len(glob.glob(bompath+"UA01D_Data*")[0]))
bomtrace = np.empty(len(gpatsyrs),dtype=strwdth)

# *********************************************
#  temp files
# *********************************************
#temp file to delimit bom sounding files
f = open(bompath+"ftmp1.txt", 'w')
f.close()

def geteralevs(erapath, fname, levname):
    import numpy as np
    from nco import Nco  # for importing netCDF variables
    nco = Nco()          # for calling nco instead of Nco
    ifile = os.path.join(erapath,fname)
    lev = np.array(nco.ncks(input=ifile, returnArray=levname)) #extract Era Int Lev points
    lev = lev.astype(np.float)
    lev[:] = [x / 100 for x in lev] # convert from Pa to hPa
    return lev

# get EraInt target pressure levels for later interpolation
erapath = "/Users/bunnr/1_DATA/eraint_reanalysis/nc/upper"
eraname = "T_RH_GH_20080101_00.nc"
levname = "lev"
lev = geteralevs(erapath, eraname, levname)

count = 0

for ii in range(len(dfsitedata)):

    # *********************************************
    # get BoM sounding data - sounding file names 
    # have structure UA01D_Data_["BoM Station ID"]_[YYYY]_43103838703834.txt
    # *********************************************
    bomtracefull = glob.glob(bompath+"UA01D_Data_"+str(dfsitedata['BoM Station ID'][ii])+"*")
    
    # restrict all available sounding files to site and years where gpats is available
    for d in range(len(bomtracefull)):
        for e in range(len(gpatsyrs)):
            if "_"+gpatsyrs[e]+"_" in str(bomtracefull[d]):
                bomtrace[e] = bomtracefull[d]
    
    for jj in range(len(bomtrace)):
        sndyr = bomtrace[jj][-23:-19]
#         print sndyr
        
        # *********************************************
        # get ts dates per site per year 
        # *********************************************
        dfts = dfthermo[(dfthermo["Station Name"] == statname[ii]) & (dfthermo["datestamp"].dt.year ==  int(sndyr))]

#         print "len(dfts)",len(dfts)
        
        if dfts.empty:
            print "No gpats at ", statname[ii], " for year ",sndyr
            del dfts
            continue

        for kk in dfts.index:
            pres, temp, dwpt, hght = gettrace.bom(bomtrace[jj],str(dfts["datestamp"][kk])) 
            
            if len(pres)==0:
                print "No sounding data at "+str(dfts["datestamp"][kk])+" for\n"+bomtrace[jj]
                del pres, temp, dwpt, hght
                continue
            
            temp,Tlen = interp.interpZ(pres,lev,temp)
            dwpt,TDlen = interp.interpZ(pres,lev,dwpt)
            hght,GHlen = interp.interpZ(pres,lev,hght)

            maxlen = max(Tlen, TDlen, GHlen)
            pres = lev[:maxlen] # restrict to available interp data only
#             print "preslength after interp", len(pres)
            print "doing sounding "+str(dfts["Station Name"][kk])+" " +str(dfts["datestamp"][kk])+str(kk)+" of "+str(len(dfthermo))
            if len(pres)<4:
                print "Very little data at "+str(dfts["datestamp"][kk])+" for\n"+bomtrace[jj]
                del pres, temp, dwpt, hght
                continue
            if len(pres)==0:
                print "No interp sounding data at "+str(dfts["datestamp"][kk])+" for\n"+bomtrace[jj]
                del pres, temp, dwpt, hght
                continue
            if (kk==92)|(kk==83):
                # NOTE dfthermo ix=83 is a Melb case and potentially an ES - GGRRR
                print "skipping due to Skewt MUCAPE crapping itself"
                del pres, temp, dwpt, hght
                continue                
              
            # Since each year of soundin data has the previous yyyy/12/31 data in it,
            # must get the yyyy-1 file, then get data
            if dfts["datestamp"][kk].date() != dt.date(dfts["datestamp"][kk].year,12,31):
                
                dfthermo = thermoindices.populate(pres,temp,dwpt,hght,dfthermo,kk )
                
#                 print dfthermo[(dfthermo["LevArr Len"]==Tlen)]
#                 sys.exit()
                
            # Since each year of soundin data has the previous yyyy/12/31 data in it,
            # must get the yyyy-1 file, then get data
            else:
                print str(bomtrace[jj - 1])
                print dfts["datestamp"][kk].date(), dt.date(dfts["datestamp"][kk].year,12,31)
#                 print "EXITING!!!!!!!"
#                 sys.exit()
                
            count = count +1 
#             print "doing sounding ", count, " of ", len(dfthermo)
                # end if ix[pp] loop - for !end & end bomtrace file
            # end kk loop - dfts chopped to site and year
        # end jj loop - bomtrace per site per year list
    # end ii loop  - city list 

# cleanup
# del dftrace, dfthermo

# sort by name and date
# dfthermo = dfthermo.sort(["Station Name", "datestamp"])
dfthermo = dfthermo.sort(["datestamp"])

# drop missing rows at end of dfthermo
dfthermo1 = dfthermo.dropna(subset=["TD MUCAPE (C)"]) 

# # SAVING dfthermo Dataframe PER STATION PER Year to file
dfthermo.to_csv(outpath+"dfthermo_gpats_bom_eralevs.csv", header = "true")
metadata.main(outpath,"dfthermo_gpats_bom_eralevs")

# # SAVING dfthermo Dataframe PER STATION PER Year to file
dfthermo1.to_csv(outpath+"dfthermo_gpats_bom_eralevs_notnan.csv", header = "true")
metadata.main(outpath,"dfthermo_gpats_bom_eralevs_notnan")

toc = time.clock()

print toc - tic

