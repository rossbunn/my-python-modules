""" Module takes BoM and Era Int dataframes, and based on era datetimes, flags BoM datetimes
    as matched to era datetimes.
"""
import pandas as pd
import numpy as np
import metadata
import maptimes_any_dhr
import os

def main(erapath,eraname,bompath,bomname,message):
    assert os.path.isfile(os.path.join(erapath,eraname)), "Era Int file or path doesn't exist"
    assert os.path.isfile(os.path.join(bompath,bomname)), "BOM file or path doesn't exist"
    assert isinstance(os.path.join(erapath,eraname), basestring), "ERA file path and file name must be a string"
    assert isinstance(os.path.join(bompath,bomname), basestring), "BOM file path and file name must be a string"
    # get existing dfthermo based on BoM TS soundings at Era Levs
    dfera = pd.read_csv(os.path.join(erapath,eraname), 
                           converters={'BoM Station ID': lambda x: str(x).zfill(6)}, 
                           parse_dates=['datestamp'], index_col = None)
    dfera = dfera.sort(["datestamp"])
    dfera = dfera.set_index(np.array(range(len(dfera))))  # relist from 0... len(df)

    # get existing dfthermo based on BoM TS soundings at Era Levs
    dfbom = pd.read_csv(os.path.join(bompath,bomname), 
                           converters={'BoM Station ID': lambda x: str(x).zfill(6)}, 
                           parse_dates=['datestamp'], index_col = None)
    dfbom = dfbom.sort(["datestamp"])
    dfbom = dfbom.set_index(np.array(range(len(dfbom))))  # relist from 0... len(df)
    
    # *********************************************
    # map bom sounding datetime stamps to closest 
    # era interim times
    # *********************************************
    sourcestamps = dfbom.datestamp
    targethours = [0,6,12,18]
    newdttimes = maptimes_any_dhr.mapto_eratimes(sourcestamps, targethours, checktimes=False)
    dftemp = pd.DataFrame(newdttimes.datestamp.tolist(), columns=["mapped dtstamps -1hr"])
    dfbom = dftemp.join(dfbom)
    del dftemp

    # adding new column to dfbom
    dftemp1 = pd.DataFrame(np.empty(len(dfbom))**np.nan, columns=["matched era sounding"])
    dfbom = dftemp1.join(dfbom)

    for i in dfbom.index:
        for j in dfera.index:
            if ((str(dfbom["mapped dtstamps -1hr"][i]) == str(dfera["datestamp"][j])) &
                (str(dfbom["Station Name"][i])         == str(dfera["Station Name"][j]))):
                dfbom.set_value(i,"matched era sounding", True)

    #  restricting dfbom to soundings matched to era interim soundings
    dfbom1 = dfbom[dfbom["matched era sounding"]==True]
    print len(dfbom),len(dfbom1), len(dfera)

    # # SAVING dfbom Dataframe to file
    dfbom1.to_csv(os.path.join(bompath,bomname), header = "true")
    metadata.main(bompath,bomname,message)