# Creates Era Int TS sounding indices by:
# 1. reading existing BoM TS soundings
# 2. maps sounding times to Era Int date/times if within 1 hour
# 3. searches EraInt .nc files for matching datetimes
# 4. on matched EraInt files, 
#  - interpolate pres, temp dwpt, hght to Capital City locations
#  - pass to SkewT, calc indices and populate dataframe dfthermo

import numpy as np
import pandas as pd       # for reading csv files and  dataframes
import glob
import time
import datetime as dt
from skewt import SkewT
from skewt import thermodynamics as thrm
import sys
import os
import thermoindices
import gettrace
import capcitydata
import convertdatestamp
import maptimes_any_dhr
import metadata

# %matplotlib inline

tic = time.clock()

# *********************************************
# paths
# *********************************************

bompath = "/Users/bunnr/1_DATA/sounding_bom_cities/"
testpath = "/Users/bunnr/python/stable_layer/stable_layer_grid_eraint/testing/"
inpath = "/Users/bunnr/2_OUTPUT/stable_layer/thermo_indices/dataframes/df_bom_eraint_resolution/"
outpath = "/Users/bunnr/2_OUTPUT/stable_layer/thermo_indices/dataframes/df_era_to_bom_max_lev_only/"

# get existing dfthermo based on BoM TS soundings at Era Levs
fname = "dfthermo_gpats_bom_eralevs_notnan.csv"
dftemp = pd.read_csv(os.path.join(inpath,fname), 
                       converters={'BoM Station ID': lambda x: str(x).zfill(6)}, 
                       parse_dates=['datestamp'], index_col = None)

# # *********************************************
# #  Reading in dates/times per capital city where 
# #  known gpats activity was recorded and soundings exist
# # *********************************************

# want to keep all station data in dftemp from 0:6, 
# but drop all indices stuff from 7: 
# (since it will be recalculated) 
# also retain existing lenArr lev - bom pressure arr length needed to restrict EraInt pres to
dftemp.rename(columns={'LevArr Len':'BoM LevArr Len'}, inplace=True)
colix = range(len(dftemp.columns))[0:3]
dftemp.drop(dftemp.columns[colix], 
                 axis=1, inplace=True)
colix = range(len(dftemp.columns))[7:] 
dftemp.drop(dftemp.columns[colix], 
                 axis=1, inplace=True)

# # SAVING dftemp Dataframe to file for later use
dftemp.to_csv(outpath+"dftemp.csv", header = "true")

# create new dfthermo
fname = "dftemp.csv"
dfthermo = thermoindices.create(outpath,fname,dtstampname="datestamp")
os.remove(outpath+"dftemp.csv")
del dftemp

# dfthermo = df
# dfthermo = df.append(dftemp)
# dfthermo = dftemp.join(df)
# fro later tracking of matched BoM soundings to EraInt files
dfthermo["matched era file"] = np.nan

# *********************************************
# map bom sounding datetime stamps to closest 
# era interim times
# *********************************************
sourcestamps = dfthermo.datestamp
targethours = [0,6,12,18]
newdttimes = maptimes_any_dhr.mapto_eratimes(sourcestamps, targethours, checktimes=False)
# maptimes.checkmappedtimes(newdttimes, sourcestamps,4)
dfthermo.drop(["datestamp"], axis=1, inplace=True)    # drop col unmatched dttimes

dftemp = pd.DataFrame(newdttimes.datestamp, columns=["datestamp"])
dfthermo = dftemp.join(dfthermo)
del dftemp

# # get list of site data only 
# dfsitedata = dfthermo.drop_duplicates(subset="Station Name", take_last = False)  # remove duplicates
# dfsitedata = dfsitedata.set_index(np.array(range(len(dfsitedata)))) # relist from 0... len(df)
# statname = dfsitedata["Station Name"]
# sitelat = dfsitedata["lat"]
# sitelon = dfsitedata["lon"]


# creating a new list of eraint files matched to thunderstorm dates and times
# create new dates list based on datetime structure in eraint filename "T_RH_SH_20130209_00.nc"
newdates = map(convertdatestamp.newdatetimes, dfthermo["datestamp"])

# testdates = ['20080101_00', '20080101_06', '20080101_12', '20080101_18', '20080102_00']
# newdates = testdates


#  get all erain fnames
# path = "/Users/bunnr/1_DATA/eraint_reanalysis/nc/upper/old_T_RH_SH" # for testing - have 66 tsfnames in 2013 
path = "/Users/bunnr/1_DATA/eraint_reanalysis/nc/upper/" 
erafs = glob.glob(os.path.join(path, "*.nc"))
#  dataframe of existing erafnames
erafnames = pd.DataFrame(erafs)
erafnames.columns = ["col1"]
index = range(len(newdates))
columns =["col1"]
tsfnms = pd.DataFrame(index=index, columns=columns)

#  get only erafnames which contain datetimes in newdates list
leni = len(newdates)
for i in range(leni):
    df = erafnames[erafnames['col1'].str.contains(newdates[i], na=False)]
    if len(df)>0:
        df1= df.set_index(np.array(range(len(df)))) # relist
        tsfnms.set_value(i,"col1",str(df1.col1[0]))
        dfthermo.set_value(i,"matched era file", True)
tsfnms = tsfnms.col1.dropna()
tsfnms = tsfnms.tolist()

# restricting dfthermo to only matched era files
dfthermo = dfthermo[dfthermo["matched era file"]==True]
dfthermo= dfthermo.set_index(np.array(range(len(dfthermo)))) # relist
if len(dfthermo)!=len(tsfnms):
    print "len(dfthermo)!=len(tsfnms)", len(dfthermo), len(tsfnms)
    sys.exit()

#  checking
# print tsfnms[0:3]
# # print dfthermo["datestamp"][dfthermo["matched era file"]==True][0:3]
# print dfthermo["datestamp"][0:3]
# sys.exit()
# condition1 = (dfthermo["datestamp"].dt.year == 2013) & (dfthermo["datestamp"].dt.hour == 0)
# condition2 = (dfthermo["datestamp"].dt.year == 2013) & (dfthermo["datestamp"].dt.hour == 12)
# print "# ts dates in era yr 2013", len(dfthermo[(condition1) | (condition2)])
# print "newlsit", len(tsfnms) 


# *********************************************
#  temp files
# *********************************************
#temp file to delimit bom sounding files
f = open(bompath+"ftmp1.txt", 'w')
f.close()


lenii = len(tsfnms)
print "length tsfnames", lenii
print "length dfthermo", len(dfthermo)
print dfthermo.index
for ii in range(lenii):
    print ii, "doing ",dfthermo["datestamp"][ii],dfthermo["Station Name"][ii], dfthermo.lat[ii], dfthermo.lon[ii]

    pera, tgrid, rhgrid, ghgrid, latera, lonera = gettrace.geteradata(tsfnms[ii])
#     print pera.shape,tgrid.shape,rhgrid.shape,ghgrid.shape

    # restrict Era traces to heights of bom traces
    cap=int(dfthermo['BoM LevArr Len'][ii])
    pera=pera[:cap]
    tgrid=tgrid[:,:cap,:,:]
    rhgrid=rhgrid[:,:cap,:,:]
    ghgrid=ghgrid[:,:cap,:,:] 
#     print pera.shape,tgrid.shape,rhgrid.shape,ghgrid.shape

    
#     print len(pera), tgrid.shape, rhgrid.shape, ghgrid.shape, latera.shape, lonera.shape
    
    x0, x1, y0, y1 =  gettrace.getsurr(dfthermo.lat[ii], dfthermo.lon[ii], latera, lonera)
        
    pres, temp, rh, hght = gettrace.interpxy(dfthermo.lat[ii], dfthermo.lon[ii],x0, x1, y0, y1, pera, tgrid, rhgrid, latera, lonera, ghgrid)

    dwpt = gettrace.tdtrace(pres, temp, rh)

    if (ii==62)|(ii==226):
        print "skipping due to Skewt MUCAPE crapping itself... saturated deep layer?"
        print "not doing", dfthermo["datestamp"][ii],dfthermo["Station Name"][ii]
        dfthermo.set_value(ii,"matched era file", False)
        del pres, temp, dwpt, hght
        continue                
        
    dfthermo = thermoindices.populate(pres,temp,dwpt,hght,dfthermo,ii )

    # end ii loop  - city list 

# cleanup
# del dftrace, dfthermo

# rename columns
dfthermo.rename(columns={'LevArr Len':'Era LevArr Len'}, inplace=True)

# sort by name and date
# dfthermo = dfthermo.sort(["Station Name", "datestamp"])
dfthermo = dfthermo.sort(["datestamp"])

# drop missing rows at end of dfthermo
dfthermo1 = dfthermo.dropna(subset=["TD MUCAPE (C)"]) 

# SAVING dfthermo Dataframe to file
dfthermo.to_csv(outpath+"dfthermo_gpats_ERAINT.csv", header = "true")
metadata.main(outpath,"dfthermo_gpats_ERAINT")

# SAVING dfthermo Dataframe to file
dfthermo1.to_csv(outpath+"dfthermo_gpats_ERAINT_notnan.csv", header = "true")
metadata.main(outpath,"dfthermo_gpats_ERAINT_notnan")

toc = time.clock()

print toc - tic

