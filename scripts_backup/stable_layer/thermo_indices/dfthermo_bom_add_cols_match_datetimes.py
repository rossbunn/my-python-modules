import pandas as pd
import numpy as np
import metadata
import maptimes_any_dhr
import os

outpath = "/Users/bunnr/2_OUTPUT/stable_layer/thermo_indices/dataframes/"

 # get existing dfthermo based on BoM TS soundings at Era Levs
fname = "dfthermo_gpats_ERAINT_notnan_v2.csv"
dfera = pd.read_csv(os.path.join(outpath,fname), 
                       converters={'BoM Station ID': lambda x: str(x).zfill(6)}, 
                       parse_dates=['datestamp'], index_col = None)

# get existing dfthermo based on BoM TS soundings at Era Levs
fname = "dfthermo_gpats_bom_eralevs_notnan.csv"
dfbom = pd.read_csv(os.path.join(outpath,fname), 
                       converters={'BoM Station ID': lambda x: str(x).zfill(6)}, 
                       parse_dates=['datestamp'], index_col = None)


# *********************************************
# map bom sounding datetime stamps to closest 
# era interim times
# *********************************************
sourcestamps = dfbom.datestamp
targethours = [0,6,12,18]
newdttimes = maptimes_any_dhr.mapto_eratimes(sourcestamps, targethours, checktimes=False)
dftemp = pd.DataFrame(newdttimes.datestamp.tolist(), columns=["mapped dtstamps -1hr"])
dfbom = dftemp.join(dfbom)
del dftemp

# clean dfbom
dfbom.drop(dfbom.columns[[1,2,3]], 
                 axis=1, inplace=True)

# adding new column to dfbom
dftemp1 = pd.DataFrame(np.empty(len(dfbom))**np.nan, columns=["matched era sounding"])
dfbom = dftemp1.join(dfbom)

for i in dfbom.index:
    for j in dfera.index:
#         if dfbom["datestamp"][i].str.contains(dfera["datestamp"][j], na=False):
        if (dfbom["mapped dtstamps -1hr"][i] == dfera.datestamp[j]):
            dfbom.set_value(i,"matched era sounding", True)

#  restricting dfbom to soundings matched to era interim soundings
dfbom1 = dfbom[dfbom["matched era sounding"]==True]
print len(dfbom),len(dfbom1), len(dfera)

# # SAVING dfbom Dataframe to file
dfbom1.to_csv(outpath+"dfthermo_gpats_bom_eralevs_notnan_matched_era_v2.csv", header = "true")
metadata.main(outpath,"dfthermo_gpats_bom_eralevs_notnan_matched_era_v2")