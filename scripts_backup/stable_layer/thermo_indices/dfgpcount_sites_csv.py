# **************************************************************************************************************************
# GPATS read in lightning data and create datetime columns to dataframe, then: 
# (a) read in gpats data per month
# (b) df restrict to small lat lon box def by sitelatlon +-x_kmtodeg... will be all times
# (c) df1 ... restrict to sounding time +- y_min_asper_pwx (+-1 hour)
# (d) len(df1) = count[k]    per sounding date
# (e) get rid of counted gpats in df
# **************************************************************************************************************************

import pandas as pd
import numpy as np
import datetime as dt
import jdcal
import time
import glob
import sys

tic = time.clock()



# *********************************************
# paths
# *********************************************
bompath = "/Users/bunnr/1_DATA/sounding_uwyoming/"
testpath = "/Users/bunnr/python/stable_layer/stable_layer_grid_eraint/testing/"
pwxpath = "/Users/bunnr/1_DATA/present_weather_obs/"
outpath = "/Users/bunnr/2_OUTPUT/stable_layer/thermodynamic_indices/"

# *********************************************
# wmoids for sites where sounding data exists
# *********************************************
sites = glob.glob(bompath+"9*")
#sites = glob.glob(bompath+"94866")    # melbourne only
#sites = glob.glob(testpath+"9*")
wmoidsite = np.zeros(len(sites), dtype=object)
for i in range(len(sites)):
    wmoidsite[i] = str(sites[i])[-5:]
    
    
# *********************************************
# get station data:
# converters change every bomid to string and 
# pads with one or 2 "0"s, ie all 6 digit numbers
# leading zeros are needed in pwx data files
# *********************************************
# pwxstations = pd.read_csv(pwxpath+"info/capital_city_data_2sites.csv", 
#                           converters={'BoM Station Number': lambda x: str(x).zfill(6)},
#                           index_col=False)
pwxstations = pd.read_csv(pwxpath+"info/capital_city_data.csv", 
                          converters={'BoM Station Number': lambda x: str(x).zfill(6)},
                          index_col=False)
# *********************************************
# define other station data arrays
# *********************************************
statname = np.zeros(len(pwxstations), dtype=object)*np.nan
wmoid = np.zeros(len(pwxstations), dtype=object)*np.nan
bomid = np.zeros(len(pwxstations), dtype=object)*np.nan
lat = np.zeros(len(pwxstations), dtype=object)*np.nan
lon = np.zeros(len(pwxstations), dtype=object)*np.nan


for i in range(len(wmoidsite)): 
    for j in range(len(pwxstations)):
        if str(pwxstations.loc[j,"WMO Number"]).strip() in wmoidsite[i]:
            statname[j] = str(pwxstations.loc[j,"Station Name"]).rstrip()
            wmoid[j] = str(pwxstations.loc[j,"WMO Number"]).strip()
            bomid[j] = str(pwxstations.loc[j,"BoM Station Number"]).strip()
            lat[j] =  str(pwxstations.loc[j,"Lat"]).strip()
            lon[j] = str(pwxstations.loc[j,"Lon"]).strip()            




# *********************************************
# sounding data
# *********************************************
# # test sounding datetime
# datestr = "YMML Observations at 12Z 30 Nov 2012"
# #datestr = "YMML Observations at 12Z 25 Sep 2008"

# yr = datestr[-4:len(datestr)]
# mt = datestr[-8:-5]
# dy = datestr[-11:-9]
# hr = datestr[-15:-13]

# format = "%Y%b%d%H"
# sndtime = [pd.to_datetime(yr.zfill(2)+mt+dy.zfill(2)+hr.zfill(2), format=format)]

sndtime = [0,9,12,21]




# convert name month to integer with leading zero padding
# mtnum = str(dt.datetime.strptime(mt,"%b").month).zfill(2)

# input paths
# ingpats = "/Users/bunnr/1_DATA/gpats/gpats_full/"+yr+mtnum+".txt"
ingpats = glob.glob("/Users/bunnr/1_DATA/gpats/gpats_full/*.txt")
# ingpats = glob.glob("/Users/bunnr/1_DATA/gpats/largesample/*.txt")

# output paths
testpath = "/Users/bunnr/python/stable_layer/stable_layer_grid_eraint/testing/"
outpath = "/Users/bunnr/2_OUTPUT/stable_layer/thermo_indices/dataframes/"

#empty dataframes for later filling
dfgpcount = pd.DataFrame()
dfgpats = pd.DataFrame()
dftmp1 = pd.DataFrame()

# get gpats data per site per time period
for ii in range(len(ingpats)):
    dfgpfull = pd.read_csv(ingpats[ii], sep=r",", header = None,
                           names=["y","m", "d", "h", "mm", "s", "fs", "Lat", "Lon", "Polarity"])
    for jj in range(len(sndtime)):
        for kk in range(len(bomid)):
            # define box around sounding site
            # dist should be as per dist of surrounding stations for T consistancy 
            # chosen as ~25km  CHANGE THIS??????
            dist= 0.25  
            xmin=float(lon[kk]) - dist
            xmax=float(lon[kk]) + dist
            ymin=float(lat[kk]) - dist
            ymax=float(lat[kk]) + dist


    #             dfgpfull.h = dfgpfull.h.astype(int)
    #             print dfgpfull.dtypes

            t = sndtime[jj]

            if t == 0:
                # restricting gpats to box around the station site
                dftmp1 = dfgpfull[(dfgpfull.Lon >= xmin) & (dfgpfull.Lon <= xmax) &
                                  (dfgpfull.Lat >= ymin) & (dfgpfull.Lat <= ymax) &
                                  (dfgpfull.h == 23) | 
                                  (dfgpfull.Lon >= xmin) & (dfgpfull.Lon <= xmax) &
                                  (dfgpfull.Lat >= ymin) & (dfgpfull.Lat <= ymax) &
                                  (dfgpfull.h == t) ] 

            else:
                dftmp1 = dfgpfull[(dfgpfull.Lon >= xmin) & (dfgpfull.Lon <= xmax) &
                                  (dfgpfull.Lat >= ymin) & (dfgpfull.Lat <= ymax) & 
                                  (dfgpfull.h == (t-1)) | 
                                  (dfgpfull.Lon >= xmin) & (dfgpfull.Lon <= xmax) &
                                  (dfgpfull.Lat >= ymin) & (dfgpfull.Lat <= ymax) & 
                                  (dfgpfull.h == t) ] 

            dfgpats = dfgpats.append(dftmp1)
            del dftmp1
    print "dfgpfull", ingpats[ii], len(dfgpfull), len(dfgpats)
    del dfgpfull
dfgpchop = dfgpats
print "chopped dfgpfull", len(dfgpchop)       

del dfgpats

for jj in range(len(sndtime)):
    for kk in range(len(bomid)):
        # define box around sounding site
        # dist should be as per dist of surrounding stations for T consistancy 
        # chosen as ~25km  CHANGE THIS??????
        dist= 0.25  
        xmin=float(lon[kk]) - dist
        xmax=float(lon[kk]) + dist
        ymin=float(lat[kk]) - dist
        ymax=float(lat[kk]) + dist


#             dfgpfull.h = dfgpfull.h.astype(int)
#             print dfgpfull.dtypes

        t = sndtime[jj]

        if t == 0:
            # restricting gpats to box around the station site
            dfgpats = dfgpchop[(dfgpchop.Lon >= xmin) & (dfgpchop.Lon <= xmax) &
                               (dfgpchop.Lat >= ymin) & (dfgpchop.Lat <= ymax) &
                               (dfgpchop.h == 23) | 
                               (dfgpchop.Lon >= xmin) & (dfgpchop.Lon <= xmax) &
                               (dfgpchop.Lat >= ymin) & (dfgpchop.Lat <= ymax) &
                               (dfgpchop.h == t) ] 

#                 dfgpats = dfgpats.append(dftmp1)
#                 del dftmp1
        else:
            dfgpats = dfgpchop[(dfgpchop.Lon >= xmin) & (dfgpchop.Lon <= xmax) &
                               (dfgpchop.Lat >= ymin) & (dfgpchop.Lat <= ymax) & 
                               (dfgpchop.h == (t-1)) | 
                               (dfgpchop.Lon >= xmin) & (dfgpchop.Lon <= xmax) &
                               (dfgpchop.Lat >= ymin) & (dfgpchop.Lat <= ymax) & 
                               (dfgpchop.h == t) ] 

#                 dfgpats = dfgpats.append(dftmp1)
#                 del dftmp1

        print "dfgpats length", len(dfgpats)
        if dfgpats.empty:
            del dfgpats
            continue
#             print statname[kk]
#             print len(dfgpfull), len(dfgpats), len(dfgpfull) -len(dfgpats)
        tic1 = time.clock()
        print "performing removal of sliced rows"
        for i in dfgpats.index:
            dfgpchop = dfgpchop.drop([i])
        toc1 = time.clock()
        print toc1 - tic1
        
        # calc datetime stamps for remaining gpats
        # get date part and remove dups

        # Relabel dfgpats from 0 to length of the list...
        dfgpats = dfgpats.set_index(np.array(range(len(dfgpats))))    

        # Define string format for datetime conversion, and empty lists to be filled.
        gpatsformat = "%Y%m%d%H"
        gptimestamp = np.zeros(len(dfgpats), dtype=object)

        # for rows in dflis dataframe, convert datetimes to strings. 
        for i in range(len(dfgpats)): 
            gptimestamp[i] = pd.to_datetime(str(int(dfgpats.y[i])).zfill(2)+str(int(dfgpats.m[i])).zfill(2)+\
                                            str(int(dfgpats.d[i])).zfill(2)+str(int(dfgpats.h[i])).zfill(2),
                                            format=gpatsformat)             

        # create temp df with timestamp data
        dftemp = pd.DataFrame(gptimestamp.tolist(), columns=["dtstamp"])

        # create array of strings and insert to dfgpcount
        strwdth = "|S"+str(len(str(statname[kk])))
        a=np.empty(len(dftemp), dtype=strwdth); a.fill(str(statname[kk]))
        dftemp.insert(1, "Station Name", a.tolist())
        del a, strwdth

        # create array of strings and insert to dfgpcount
        strwdth = "|S"+str(len(str(bomid[kk])))
        a=np.empty(len(dftemp), dtype=strwdth); a.fill(str(bomid[kk]))
        dftemp.insert(2, "BoM Station ID", a.tolist())
        del a, strwdth

        # create array of strings and insert to dfgpcount
        strwdth = "|S"+str(len(str(wmoid[kk])))
        a=np.empty(len(dftemp), dtype=strwdth); a.fill(str(wmoid[kk]))
        dftemp.insert(3, "WMO ID", a.tolist())
        del a, strwdth
        
        # create array of strings and insert to dfgpcount
        strwdth = "|S"+str(len(str(lat[kk])))
        a=np.empty(len(dftemp), dtype=strwdth); a.fill(str(lat[kk]))
        dftemp.insert(3, "lat", a.tolist())
        del a, strwdth
        
        # create array of strings and insert to dfgpcount
        strwdth = "|S"+str(len(str(lon[kk])))
        a=np.empty(len(dftemp), dtype=strwdth); a.fill(str(lon[kk]))
        dftemp.insert(3, "lon", a.tolist())
        del a, strwdth

        dftemp = dftemp.drop_duplicates(subset="dtstamp", take_last = False)  # remove duplicates
        
        dfgpcount = dfgpcount.append(dftemp)

        # checking dfgpcount isn't blowing out to a humungus size
        if len(dfgpcount) > len(statname)*365*len(sndtime)*(2014-1994):
            print "length of dfgpcount is too large!!!"+str(len(dfgpcount)) ; sys.exit()                      

        del gptimestamp, dfgpats, dftemp # cleanup

    # end kk loop for all sites
# end jj loop for all sounding times


dfgpcount = dfgpcount.set_index(np.array(range(len(dfgpcount))))       # relist rows 0...N  
print "len(dfgpcount): ", len(dfgpcount)

import metadata

# SAVING gpcount Dataframe to file
dfgpcount.to_csv(outpath+"dfgpcount_sites_v2.csv", header = "true", index=False)
metadata.main(outpath,"dfgpcount_sites_v2")

           
toc = time.clock()
print toc - tic
