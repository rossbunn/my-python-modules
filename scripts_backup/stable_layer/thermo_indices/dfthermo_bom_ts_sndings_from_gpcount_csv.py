# Re-running for all sites with known gpats activity, for all capital city sites using BoM sounding data (not Uni Wyoming)
#
# Restricting theta and thetaE to 1000-600hPa (most others restrict to lowest 300hpa ie 1000-700, 
#
# NOTE -MUCAPE 1000-500hpa


import numpy as np
import pandas as pd       # for reading csv files and  dataframes
import glob
import time
import datetime as dt
from skewt import SkewT
from skewt import thermodynamics as thrm
import sys
import os
import thermoindices
import gettrace

#%matplotlib inline

tic = time.clock()

# *********************************************
# paths
# *********************************************

bompath = "/Users/bunnr/1_DATA/sounding_bom_cities/"
testpath = "/Users/bunnr/python/stable_layer/stable_layer_grid_eraint/testing/"
outpath = "/Users/bunnr/2_OUTPUT/stable_layer/thermo_indices/dataframes/"

# *********************************************
# years where gpats data is available
# *********************************************
gpatsyrs = np.array(["2008", "2009", "2010", "2011", "2012", "2013", "2014"], dtype=str)

# # *********************************************
# #  Reading in dates/times per capital city where 
# #  known gpats activity was recorded
# # *********************************************
# files
fname = "dfgpcount_sites_v2.csv"

dfthermo = thermoindices.create(outpath,fname)


# get list of site data only
dfsitedata = dfthermo.drop_duplicates(subset="Station Name", take_last = False)  # remove duplicates
dfsitedata = dfsitedata.set_index(np.array(range(len(dfsitedata))))                              # relist from 0... len(df)
statname = dfsitedata["Station Name"]

# *********************************************
#  arrays needed
# *********************************************
strwdth = "|S"+str(len(glob.glob(bompath+"UA01D_Data*")[0]))
bomtrace = np.empty(len(gpatsyrs),dtype=strwdth)

# *********************************************
#  temp files
# *********************************************
#temp file to delimit bom sounding files
f = open(bompath+"ftmp1.txt", 'w')
f.close()
 

for ii in range(len(dfsitedata)):

    # *********************************************
    # get BoM sounding data - sounding file names 
    # have structure UA01D_Data_["BoM Station ID"]_[YYYY]_43103838703834.txt
    # *********************************************
    bomtracefull = glob.glob(bompath+"UA01D_Data_"+str(dfsitedata['BoM Station ID'][ii])+"*")
    
    # restrict all available sounding files to site and years where gpats is available
    for d in range(len(bomtracefull)):
        for e in range(len(gpatsyrs)):
            if "_"+gpatsyrs[e]+"_" in str(bomtracefull[d]):
                bomtrace[e] = bomtracefull[d]
    
    for jj in range(len(bomtrace)):
        sndyr = bomtrace[jj][-23:-19]
        print sndyr
        
        # *********************************************
        # get ts dates per site per year 
        # *********************************************
        dfts = dfthermo[(dfthermo["Station Name"] == statname[ii]) & (dfthermo["datestamp"].dt.year ==  int(sndyr))]

        print "len(dfts)",len(dfts)
        
        if dfts.empty:
            print "No gpats at ", statname[ii], " for year ",sndyr
            del dfts
            continue

        for kk in dfts.index:
            pres, temp, dwpt, hght = gettrace.bom(bomtrace[jj],str(dfts["datestamp"][kk])) 
            
       
            if len(pres)==0:
                print "No sounding data at "+str(dfts["datestamp"][kk])+" for\n"+bomtrace[jj]
                del pres, temp, dwpt, hght
                continue
            
            
            # Since each year of soundin data has the previous yyyy/12/31 data in it,
            # must get the yyyy-1 file, then get data
            if dfts["datestamp"][kk].date() != dt.date(dfts["datestamp"][kk].year,12,31):

                dfthermo = thermoindices.populate(pres,temp,dwpt,hght,dfthermo,kk )
                
#                 print dfthermo
#                 sys.exit()
                
            # Since each year of soundin data has the previous yyyy/12/31 data in it,
            # must get the yyyy-1 file, then get data
            else:
                print str(bomtrace[jj - 1])
                print dfts["datestamp"][kk].date(), dt.date(dfts["datestamp"][kk].year,12,31)
#                 print "EXITING!!!!!!!"
#                 sys.exit()
                    
                # end if ix[pp] loop - for !end & end bomtrace file
            # end kk loop - dfts chopped to site and year
        # end jj loop - bomtrace per site per year list
    # end ii loop  - city list 

# cleanup
# del dftrace, dfthermo

# sort by name and date
# dfthermo = dfthermo.sort(["Station Name", "datestamp"])
dfthermo = dfthermo.sort(["datestamp"])

# drop missing rows at end of dfthermo
dfthermo1 = dfthermo.dropna(subset=["TD MUCAPE (C)"]) 

# # SAVING dfthermo Dataframe PER STATION PER Year to file
dfthermo.to_csv(outpath+"dfthermo_gpats_bom_v3.csv", header = "true")

# # SAVING dfthermo Dataframe PER STATION PER Year to file
dfthermo1.to_csv(outpath+"dfthermo_gpats_bom_notnan_v3.csv", header = "true")

toc = time.clock()

print toc - tic

