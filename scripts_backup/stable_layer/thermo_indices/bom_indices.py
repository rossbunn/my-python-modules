# Re-running for all sites with known gpats activity, for all capital city sites using BoM sounding data (not Uni Wyoming)
#
# Restricting theta and thetaE to 1000-600hPa (most others restrict to lowest 300hpa ie 1000-700, 
#
# NOTE -MUCAPE 1000-500hpa


import numpy as np
import pandas as pd       # for reading csv files and  dataframes
import glob
import time
import datetime as dt
from skewt import SkewT
from skewt import thermodynamics as thrm
import sys
import os
import thermoindices
import gettrace
import metadata
import maptimes_any_dhr
import interp

#%matplotlib inline

tic = time.clock()

# *********************************************
# paths
# *********************************************

bompath = "/Users/bunnr/1_DATA/sounding_bom_cities/"
testpath = "/Users/bunnr/2_OUTPUT/stable_layer/thermo_indices/dataframes/testing"
inpath = "/Users/bunnr/2_OUTPUT/stable_layer/thermo_indices/dataframes/df_gpats_at_snd_locations_times"
outpath = "/Users/bunnr/2_OUTPUT/stable_layer/thermo_indices/dataframes/"

# *********************************************
# years where gpats data is available
# *********************************************
gpatsyrs = np.array(["2008", "2009", "2010", "2011", "2012", "2013", "2014"], dtype=str)

# # *********************************************
# #  Reading in dates/times per capital city where 
# #  known gpats activity was recorded
# # *********************************************
# files
fname = "dfgpcount_sites_v2.csv"

dfthermo = thermoindices.create(inpath,fname,dtstampname="datestamp")
dfthermo = dfthermo.sort(["datestamp"])
dfthermo = dfthermo.set_index(np.array(range(len(dfthermo))))  # relist from 0... len(df)

# get list of site data only
dfsitedata = dfthermo.drop_duplicates(subset="Station Name", take_last = False)  # remove duplicates
dfsitedata = dfsitedata.set_index(np.array(range(len(dfsitedata))))              # relist from 0... len(df)

# *********************************************
#  arrays needed
# *********************************************
strwdth = "|S"+str(len(glob.glob(bompath+"UA01D_Data*")[0]))
bomtrace = np.empty(len(gpatsyrs),dtype=strwdth)

# *********************************************
#  temp files
# *********************************************
#temp file to delimit bom sounding files
f = open(os.path.join(bompath,"ftmp1.txt"), 'w')
f.close()

def geteralevs(erapath, fname, levname):
    import numpy as np
    from nco import Nco  # for importing netCDF variables
    nco = Nco()          # for calling nco instead of Nco
    ifile = os.path.join(erapath,fname)
    lev = np.array(nco.ncks(input=ifile, returnArray=levname)) #extract Era Int Lev points
    lev = lev.astype(np.float)
    lev[:] = [x / 100 for x in lev] # convert from Pa to hPa
    return lev

# get EraInt target pressure levels for later interpolation
erapath = "/Users/bunnr/1_DATA/eraint_reanalysis/nc/upper"
eraname = "T_RH_GH_20080101_00.nc"
levname = "lev"
lev = geteralevs(erapath, eraname, levname)

# define value of missing data, either -9999 (needed for sharppy, or NaN needed for skewt)
fillnan=True # sharppy
# fillnan=False # skewt

#testing
# dfsitedata = dfsitedata[dfsitedata["Station Name"]=="SYDNEY AIRPORT AMO"]
# dfsitedata = dfsitedata.set_index(np.array(range(len(dfsitedata))))   # relist from 0... len(df)
# print dfsitedata

lenii = len(dfsitedata)
for ii in range(lenii):

    # *********************************************
    # get BoM sounding data - sounding file names 
    # have structure UA01D_Data_["BoM Station ID"]_[YYYY]_43103838703834.txt
    # *********************************************
    bomtracefull = glob.glob(bompath+"UA01D_Data_"+str(dfsitedata['BoM Station ID'][ii])+"*")
    
    # restrict all available sounding files to site and years where gpats is available
    for d in range(len(bomtracefull)):
        for e in range(len(gpatsyrs)):
            if "_"+gpatsyrs[e]+"_" in str(bomtracefull[d]):
                bomtrace[e] = bomtracefull[d]
                
    lenjj = len(bomtrace)
    for jj in range(lenjj):
        sndyr = bomtrace[jj][-23:-19]
        print sndyr
        
        # *********************************************
        # get ts dates per site per year 
        # *********************************************
        dfts = dfthermo[(dfthermo["Station Name"] == dfsitedata["Station Name"][ii]) & 
                        (dfthermo["datestamp"].dt.year ==  int(sndyr))]

        print "len(dfts)",len(dfts)
        
        if dfts.empty:
            print "No gpats at ", dfsitedata["Station Name"][ii], " for year ",sndyr
            del dfts
            continue

        for kk in dfts.index:
            pres, temp, dwpt, hght = gettrace.bom(bomtrace[jj],str(dfts["datestamp"][kk]),fillnan=fillnan) 
            
            if len(pres)==0:
                print "No sounding data at "+str(dfts["datestamp"][kk])+" for\n"+bomtrace[jj]
                del pres, temp, dwpt, hght
                continue
            
            # *********************************************
            # reduce BoM ver resolution to Era Int
            # *********************************************
            pb_res       = lev
            x=pd.DataFrame(pb_res); x = x[x>0.0]; x =x.dropna(subset=[[0]]) # dropping nan & -9999 

            if len(x)<4:
                print "Very little PRESSURE data for "+str(dfts["datestamp"][kk])+" for "+str(dfts["Station Name"][kk])
                del pb_res, x
                continue
            presbom_res = pd.DataFrame({"presbom_res" : pb_res})
            
            tb_res,Tlen  = interp.interpZ(pres,lev,temp,remnan=False)
            x=pd.DataFrame(tb_res); x = x[x>0.0]; x =x.dropna(subset=[[0]]) # dropping nan & -9999
            if len(x)<4:
                print "Very little TEMPERATURE data at "+str(dfts["datestamp"][kk])+" for "+str(dfts["Station Name"][kk])
                del tb_res, x
                continue
            tempbom_res = pd.DataFrame({"tempbom_res" : tb_res})
            
            db_res,TDlen = interp.interpZ(pres,lev,dwpt,remnan=False)
            x=pd.DataFrame(db_res); x = x[x>0.0]; x =x.dropna(subset=[[0]]) # dropping nan & -9999
            if len(x)<4:
                print "Very little DEWPOINT data at "+str(dfts["datestamp"][kk])+" for "+str(dfts["Station Name"][kk])
                del db_res, x
                continue
            dwptbom_res = pd.DataFrame({"dwptbom_res" : db_res})
            
            hb_res,GHlen = interp.interpZ(pres,lev,hght,remnan=False)
            x=pd.DataFrame(hb_res); x = x[x>0.0]; x =x.dropna(subset=[[0]]) # dropping nan & -9999
            if len(x)<4:
                print "Very little GPHEIGHT data at "+str(dfts["datestamp"][kk])+" for "+str(dfts["Station Name"][kk])
                del hb_res, x
                continue
            hghtbom_res = pd.DataFrame({"hghtbom_res" : hb_res})
            dfbom_res = pd.concat([presbom_res, tempbom_res, dwptbom_res, hghtbom_res], axis=1)
            # restrict dataframe to rows without missing geopotential heights (SkewT hates it)
            dfbom_res = dfbom_res.dropna(subset = ["hghtbom_res"])
            presbom_res = dfbom_res["presbom_res"]
            tempbom_res = dfbom_res["tempbom_res"]
            dwptbom_res = dfbom_res["dwptbom_res"]
            hghtbom_res = dfbom_res["hghtbom_res"]
            print "doing sounding "+str(dfts["Station Name"][kk])+" " +str(dfts["datestamp"][kk])+" "+str(kk)+" of "+str(len(dfthermo))
        

#             if str(dfts["datestamp"][kk])=="2008-03-05 11:00:00": # TESTING
            if (str(dfts["datestamp"][kk])=="2012-06-19 23:00:00")| \
               (str(dfts["datestamp"][kk])=="2008-03-20 11:00:00")| \
               (str(dfts["datestamp"][kk])=="2008-03-25 12:00:00")| \
               (str(dfts["datestamp"][kk])=="2008-03-29 11:00:00")| \
               (str(dfts["datestamp"][kk])=="2008-07-27 11:00:00")| \
               (str(dfts["datestamp"][kk])=="2008-10-03 11:00:00")| \
               (str(dfts["datestamp"][kk])=="2008-10-04 23:00:00")| \
               (str(dfts["datestamp"][kk])=="2008-10-21 23:00:00")| \
               (str(dfts["datestamp"][kk])=="2008-11-19 11:00:00")| \
               (str(dfts["datestamp"][kk])=="2008-03-10 11:00:00"):
                print "skipping due to SHARPpy MUCAPE crapping itself "+ \
                      str(dfts["datestamp"][kk]) + " "+ str(dfts["Station Name"][kk])+" "+str(kk)
                del presbom_res, tempbom_res, dwptbom_res, hghtbom_res
                continue                
              
            # Since each year of soundin data has the previous yyyy/12/31 data in it,
            # must get the yyyy-1 file, then get data
            if dfts["datestamp"][kk].date() != dt.date(dfts["datestamp"][kk].year,12,31):
                
                dfthermo = thermoindices.populatesharppy(presbom_res,tempbom_res,dwptbom_res,hghtbom_res,dfthermo,kk )
#                 print dfthermo[(dfthermo["LevArr Len"]==Tlen)]
#                 print dfthermo.iloc[kk]
#                 sys.exit()
                
            # Since each year of soundin data has the previous yyyy/12/31 data in it,
            # must get the yyyy-1 file, then get data
            else:
                print str(bomtrace[jj - 1])
                print dfts["datestamp"][kk].date(), dt.date(dfts["datestamp"][kk].year,12,31)

            # end kk loop - dfts chopped to site and year
        # end jj loop - bomtrace per site per year list
    # end ii loop  - city list 


# sort by date
dfthermo = dfthermo.sort(["datestamp"])

# drop missing rows at end of dfthermo
dfthermo1 = dfthermo.dropna(subset=["TD MUCAPE (C)"]) 
# 
# # # SAVING dfthermo Dataframe PER STATION PER Year to file
# dfthermo.to_csv(os.path.join(outpath,"cities_BOMsnd_TSthermoindices.csv"), header = "true")
# toc = time.clock()
# message="\n Script takes "+str(toc - tic)+" seconds to run.\nThermodynamic indices from Era Interim based soundings from Australian Capital Cities \nat thunderstorm locations and times."
# metadata.main(outpath,"cities_BOMsnd_TSthermoindices", message=message)

# testing
outpath=testpath

# # SAVING dfthermo Dataframe PER STATION PER Year to file
dfthermo1.to_csv(os.path.join(outpath,"cities_BOMsnd_TSthermoindices_rmnan.csv"), header = "true")
toc = time.clock()
message="\n Script takes "+str(toc - tic)+" seconds to run.\nThermodynamic indices from Era Interim based soundings from Australian Capital Cities \nat thunderstorm locations and times. Missing rows in MUCAPE Temperature have been removed."
metadata.main(outpath,"cities_BOMsnd_TSthermoindices_rmnan", message=message)

toc = time.clock()

print toc - tic

