# **************************************************************************************************************************
# GPATS read in lightning data and create datetime columns to dataframe, then: 
# (a) read in gpats data per month
# (b) df restrict to small lat lon box ... will be strikes at all times in box
# (c) parse dates to datetime stamps
# (d) sorts by strikes within each day, counts number strikes, and outputs date and number strikes to temp file
# (e) reads in temp file as dataframe, then removes all rows where strike count = 0
# (f) saves dataframe to file
# **************************************************************************************************************************

import pandas as pd
import numpy as np
import datetime as dt
import time
import glob
import sys
import os

tic = time.clock()

# *********************************************
# INPUTS
# *********************************************
testing = False
# site data
centre_lon=152.862
centre_lat=-27.668
radius = 150
xmin= 151.332
ymin= -29.012
xmax= 154.392
ymax= -26.315
bomid = ["040842"]

# *********************************************
# paths
# *********************************************
stationpath = "/Users/bunnr/1_DATA/sounding_bom_cities/info/"
gppath = "/Users/bunnr/1_DATA/gpats/gpats_full/"
outpath = "/Users/bunnr/2_OUTPUT/stable_layer/thermo_indices/dataframes/"

if testing==True:
    testpath = "/Users/bunnr/2_OUTPUT/stable_layer/thermo_indices/dataframes/testing/"
    outpath = testpath
    ingpats = glob.glob(gppath+"/200809.txt") # GPATS files
else:
    ingpats = glob.glob(gppath+"/20*.txt") #GPATS files


#empty dataframes for later filling
dfgpcount = pd.DataFrame()
dfgpats = pd.DataFrame()
dftmp1 = pd.DataFrame()

# get gpats data per site per time period
for ii in range(len(ingpats)):
    dfgpfull = pd.read_csv(ingpats[ii], sep=r",", header = None,
                           names=["y","m", "d", "h", "mm", "s", "fs", "Lat", "Lon", "Polarity"])
    for kk in range(len(bomid)):

        dftmp1 = dfgpfull[(dfgpfull.Lon >= xmin) & (dfgpfull.Lon <= xmax) &
                          (dfgpfull.Lat >= ymin) & (dfgpfull.Lat <= ymax) ]

#         a = (centre_lon-dfgpfull.Lon)**2.0 + (centre_lat-dfgpfull.Lat)**2.0
#          
#         dftmp1 = dfgpfull[ a <= radius**2.0]
        
#         if len(dftmp1)!=0:
#             print dftmp1.Lon.head(1)
#             print dftmp1.Lat.head(1)
# #             print (centre_lon-dftmp1.Lon[0])**2.0 + (centre_lat-dftmp1.Lat[0])**2.0
#             print len(dftmp1), dftmp1.iloc[0:5]
#             sys.exit()


        dfgpats = dfgpats.append(dftmp1)
        del dftmp1
    print "dfgpfull", ingpats[ii], len(dfgpfull), len(dfgpats)
    del dfgpfull

print "dfgpats", len(dfgpats) , dfgpats.iloc[-5:]      


# Relabel dfgpats from 0 to length of the list...
dfgpats = dfgpats.set_index(np.array(range(len(dfgpats))))    

# Define string format for datetime conversion, and empty lists to be filled.
gpatsformat = "%Y%m%d"
gptimestamp = np.zeros(len(dfgpats), dtype=object)

# for rows in  dataframe, convert datetimes to strings. 
for i in range(len(dfgpats)): 
    gptimestamp[i] = pd.to_datetime(str(int(dfgpats.y[i])).zfill(2)+str(int(dfgpats.m[i])).zfill(2)+\
                                    str(int(dfgpats.d[i])).zfill(2),
                                    format=gpatsformat)
# create temp df with timestamp data
# dftemp = pd.DataFrame(gptimestamp.tolist(), columns=["dtstamp"])
dftemp = pd.DataFrame(gptimestamp, columns=["dtstamp"])

# create new file to pipe dates and gpcounts to
f = open(outpath+"ftmp.txt", 'w')
f.write("date,strike count\n")
f.close()                  

startdate = dt.date(2008,3,1)
enddate = dt.date(2014,10,31)

# print startdate,enddate
d = startdate
delta = dt.timedelta(days=1)
while d <= enddate:
    gpcount = dftemp[dftemp["dtstamp"]== d ]
    f = open(os.path.join(outpath,"ftmp.txt"), 'a')
    f.write(str(d)+","+str(len(gpcount))+"\n")
    f.close() 
    d += delta

gp_bris = pd.read_csv(os.path.join(outpath,"ftmp.txt"))
print   len(gp_bris) 
gp_bris = gp_bris[gp_bris["strike count"]!=0]

print   len(gp_bris)  
              
# SAVING gpcount Dataframe to file
gp_bris.to_csv(os.path.join(outpath,"dates_gpats_brisbane.csv"), header = "true", index=False)

print os.path.join(outpath,"dates_gpats_brisbane.csv")

        
toc = time.clock()
print toc - tic
