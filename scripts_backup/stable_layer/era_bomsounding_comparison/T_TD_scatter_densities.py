# Tdiff vs TDdiff scatter plots & DENSITIES for each level 1000-500hPa, levels limitted to 500hPa, 
# since TD formula is accurate to -40degC only

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy.stats import kde
from scipy import stats

%matplotlib inline

inpath = "/Users/bunnr/2_OUTPUT/stable_layer/era_bomsounding_comparison/dataframes/"
outpath = "/Users/bunnr/2_OUTPUT/stable_layer/era_bomsounding_comparison/plots/"

# Tdiff data
tdiff = pd.read_csv(inpath+"tdiff.csv" , index_col=False)
#Rename several DataFrame columns
tdiff = tdiff.rename(columns = 
               {'1000':'lev1000','925':'lev925','850':'lev850',
                '700':'lev700','500':'lev500',})
tddiff = pd.read_csv(inpath+"tddiff.csv" , index_col=False)
tddiff = tddiff.rename(columns = 
               {'1000':'lev1000','925':'lev925','850':'lev850',
                '700':'lev700','500':'lev500',})




#df2 = df2[(np.isnan(df2.tbom)  == False)]     # retain all rows where Tbom is NOT missing
#df2 = df2.set_index(np.array(range(len(df2))))                      # relist rows 0...N

fig = plt.figure(figsize=(6,12))

plt.subplot(4, 2, 1)
m1, m2 = tdiff.lev1000, tddiff.lev1000
xmin, xmax = min(m1), max(m1)
ymin, ymax = min(m2), max(m2)
plt.axhline(0, xmin=xmin, xmax=xmax, color='black')
plt.axvline(0, ymin=ymin, ymax=ymax, color='black')
plt.axhline(0, color='black')
plt.axvline(0, color='black')
plt.scatter(tdiff.lev1000, tddiff.lev1000, s=0.3,  color='0.1')
plt.text(xmin+1, ymax-4,'Era Int \nWarm \nDry')
plt.text(xmax-5, ymax-4,'Era Int \nCool \nDry')
plt.text(xmin+1, ymin,'Era Int \nWarm \nMoist')
plt.text(xmax-5, ymin,'Era Int \nCool \nMoist')
plt.title('T and TD differences \n1000hPa')
#plt.xlabel("T Difference Tbom-Tera (C)")
plt.ylabel("TD Difference TDbom-TDera (C)")


plt.subplot(4, 2, 2)
plt.axhline(0, color='black')
plt.axvline(0, color='black')
# Perform a kernel density estimate (KDE) on the data
x, y = np.mgrid[-6:6:100j, -10:10:100j]
positions = np.vstack([x.ravel(), y.ravel()])
values = np.vstack([m1, m2])
kernel = stats.gaussian_kde(values)
f = np.reshape(kernel(positions).T, x.shape)
# Draw contour lines
cset = plt.contour(x,y,f, linewidths=2)
#plt.clabel(cset, inline=1, fontsize=10)
#plt.colorbar()
plt.title('T and TD differences \nScatter Densities \n1000hPa')
#plt.xlabel("T Difference Tbom-Tera (C)")
#plt.ylabel("TD Difference TDbom-TDera (C)")

plt.subplot(4, 2, 3)
m1, m2 = tdiff.lev925, tddiff.lev925
xmin, xmax = min(m1), max(m1)
ymin, ymax = min(m2), max(m2)
plt.axhline(0,  color='black')
plt.axvline(0,  color='black')
#plt.extent=[xmin, xmax, ymin, ymax]
plt.scatter(tdiff.lev925, tddiff.lev925, s=0.3,  color='0.1')
plt.text(xmin-3, ymax-9.5,'Era Int \nWarm \nDry')
plt.text(xmax-3, ymax-9.5,'Era Int \nCool \nDry')
plt.text(xmin-3, ymin+2,'Era Int \nWarm \nMoist')
plt.text(xmax-3, ymin+2,'Era Int \nCool \nMoist')
plt.title('T and TD differences \n925hPa')
#plt.xlabel("T Difference Tbom-Tera (C)")
plt.ylabel("TD Difference TDbom-TDera (C)")


plt.subplot(4, 2, 4)
m1, m2 = tdiff.lev925, tddiff.lev925
xmin, xmax = min(m1), max(m1)
ymin, ymax = min(m2), max(m2)
plt.axhline(0, -10, 10, color='black')
plt.axvline(0, -10, 10, color='black')
plt.extent=[-10, 10, -10, 10]
# Perform a kernel density estimate (KDE) on the data
x, y = np.mgrid[-6:6:100j, -10:10:100j]
positions = np.vstack([x.ravel(), y.ravel()])
values = np.vstack([m1, m2])
kernel = stats.gaussian_kde(values)
f = np.reshape(kernel(positions).T, x.shape)
# Draw contour lines
cset = plt.contour(x,y,f, linewidths=2)
plt.title('T and TD differences \nScatter Densities \n925hPa')
#plt.colorbar()
#plt.xlabel("T Difference Tbom-Tera (C)")
#plt.ylabel("TD Difference TDbom-TDera (C)")

plt.subplot(4, 2, 5)
m1, m2 = tdiff.lev850, tddiff.lev850
xmin, xmax = min(m1), max(m1)
ymin, ymax = min(m2), max(m2)
plt.axhline(0, xmin=xmin, xmax=xmax, color='black')
plt.axvline(0, ymin=ymin, ymax=ymax, color='black')
plt.extent=[xmin, xmax, ymin, ymax]
plt.scatter(tdiff.lev850, tddiff.lev850, s=0.3,  color='0.1')
plt.text(xmin-3, ymax-12,'Era Int \nWarm \nDry')
plt.text(xmax-2, ymax-12,'Era Int \nCool \nDry')
plt.text(xmin-3, ymin-1,'Era Int \nWarm \nMoist')
plt.text(xmax-2, ymin-1,'Era Int \nCool \nMoist')
plt.title('T and TD differences \n850hPa')
#plt.xlabel("T Difference Tbom-Tera (C)")
plt.ylabel("TD Difference TDbom-TDera (C)")


plt.subplot(4, 2, 6)
m1, m2 = tdiff.lev850, tddiff.lev850
xmin, xmax = min(m1), max(m1)
ymin, ymax = min(m2), max(m2)
plt.axhline(0, -10, 10, color='black')
plt.axvline(0, -10, 10, color='black')
plt.extent=[-10, 10, -10, 10]
# Perform a kernel density estimate (KDE) on the data
x, y = np.mgrid[-6:6:100j, -18:18:100j]
positions = np.vstack([x.ravel(), y.ravel()])
values = np.vstack([m1, m2])
kernel = stats.gaussian_kde(values)
f = np.reshape(kernel(positions).T, x.shape)
# Draw contour lines
cset = plt.contour(x,y,f, linewidths=2)
plt.title('T and TD differences \nScatter Densities \n850hPa')
#plt.colorbar()
#plt.xlabel("T Difference Tbom-Tera (C)")
#plt.ylabel("TD Difference TDbom-TDera (C)")

plt.subplot(4, 2, 7)
m1, m2 = tdiff.lev700, tddiff.lev700
xmin, xmax = min(m1), max(m1)
ymin, ymax = min(m2), max(m2)
plt.axhline(0, xmin=xmin, xmax=xmax, color='black')
plt.axvline(0, ymin=ymin, ymax=ymax, color='black')
plt.extent=[xmin, xmax, ymin, ymax]
plt.scatter(tdiff.lev700, tddiff.lev700, s=0.3,  color='0.1')
plt.text(xmin-2, ymax-12,'Era Int \nWarm \nDry')
plt.text(xmax-2, ymax-12,'Era Int \nCool \nDry')
plt.text(xmin-2, ymin-1,'Era Int \nWarm \nMoist')
plt.text(xmax-2, ymin-1,'Era Int \nCool \nMoist')
plt.title('T and TD differences \n700hPa')
plt.xlabel("T Difference Tbom-Tera (C)")
plt.ylabel("TD Difference TDbom-TDera (C)")


plt.subplot(4, 2, 8)
m1, m2 = tdiff.lev700, tddiff.lev700
xmin, xmax = min(m1), max(m1)
ymin, ymax = min(m2), max(m2)
plt.axhline(0, -10, 10, color='black')
plt.axvline(0, -10, 10, color='black')
plt.extent=[-10, 10, -10, 10]
# Perform a kernel density estimate (KDE) on the data
x, y = np.mgrid[-6:6:100j, -18:18:100j]
positions = np.vstack([x.ravel(), y.ravel()])
values = np.vstack([m1, m2])
kernel = stats.gaussian_kde(values)
f = np.reshape(kernel(positions).T, x.shape)
# Draw contour lines
cset = plt.contour(x,y,f, linewidths=2)
plt.title('T and TD differences \nScatter Densities \n700hPa')
#plt.colorbar()
plt.xlabel("T Difference Tbom-Tera (C)")
#plt.ylabel("TD Difference TDbom-TDera (C)")
fig.tight_layout()
plt.gcf().savefig(outpath+"T_TD_diff_densities.png", dpi=400)

plt.show()


