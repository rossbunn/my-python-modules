import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

%matplotlib inline

inpath = "/Users/bunnr/2_OUTPUT/stable_layer/era_bomsounding_comparison/txt/"
outpath = "/Users/bunnr/2_OUTPUT/stable_layer/era_bomsounding_comparison/plots/"

#getting data
df1 = pd.read_csv(inpath+"Tbom_Tera_1000.txt" , index_col=False)
df1.columns = ["tbom", "tera"]
df1 = df1[(np.isnan(df1.tbom)  == False)]     # retain all rows where Tbom is NOT missing
df1 = df1.set_index(np.array(range(len(df1))))                      # relist rows 0...N
#getting data
df2 = pd.read_csv(inpath+"Tbom_Tera_925.txt" , index_col=False)
df2.columns = ["tbom", "tera"]
df2 = df2[(np.isnan(df2.tbom)  == False)]     # retain all rows where Tbom is NOT missing
df2 = df2.set_index(np.array(range(len(df2))))                      # relist rows 0...N
#getting data
df3 = pd.read_csv(inpath+"Tbom_Tera_850.txt" , index_col=False)
df3.columns = ["tbom", "tera"]
df3 = df3[(np.isnan(df3.tbom)  == False)]     # retain all rows where Tbom is NOT missing
df3 = df3.set_index(np.array(range(len(df3))))                      # relist rows 0...N
#getting data
df4 = pd.read_csv(inpath+"Tbom_Tera_700.txt" , index_col=False)
df4.columns = ["tbom", "tera"]
df4 = df4[(np.isnan(df4.tbom)  == False)]     # retain all rows where Tbom is NOT missing
df4 = df4.set_index(np.array(range(len(df4))))                      # relist rows 0...N
#getting data
df5 = pd.read_csv(inpath+"Tbom_Tera_500.txt" , index_col=False)
df5.columns = ["tbom", "tera"]
df5 = df5[(np.isnan(df5.tbom)  == False)]     # retain all rows where Tbom is NOT missing
df5 = df5.set_index(np.array(range(len(df5))))                      # relist rows 0...N
#getting data
df6 = pd.read_csv(inpath+"Tbom_Tera_400.txt" , index_col=False)
df6.columns = ["tbom", "tera"]
df6 = df6[(np.isnan(df6.tbom)  == False)]     # retain all rows where Tbom is NOT missing
df6 = df6.set_index(np.array(range(len(df6))))                      # relist rows 0...N
#getting data
df7 = pd.read_csv(inpath+"Tbom_Tera_300.txt" , index_col=False)
df7.columns = ["tbom", "tera"]
df7 = df7[(np.isnan(df7.tbom)  == False)]     # retain all rows where Tbom is NOT missing
df7 = df7.set_index(np.array(range(len(df7))))                      # relist rows 0...N
#getting data
df8 = pd.read_csv(inpath+"Tbom_Tera_200.txt" , index_col=False)
df8.columns = ["tbom", "tera"]
df8 = df8[(np.isnan(df8.tbom)  == False)]     # retain all rows where Tbom is NOT missing
df8 = df8.set_index(np.array(range(len(df8))))                      # relist rows 0...N



#maxes for plotting
max1 = df1.tbom.max(axis=0) 
max2 = df1.tera.max(axis=0) 
df1max = max([max1,max2])
min1 = df1.tbom.min(axis=0) 
min2 = df1.tera.min(axis=0) 
df1min = min([min1,min2])
#maxes for plotting
max1 = df2.tbom.max(axis=0) 
max2 = df2.tera.max(axis=0) 
df2max = max([max1,max2])
min1 = df2.tbom.min(axis=0) 
min2 = df2.tera.min(axis=0) 
df2min = min([min1,min2])
#maxes for plotting
max1 = df3.tbom.max(axis=0) 
max2 = df3.tera.max(axis=0) 
df3max = max([max1,max2])
min1 = df3.tbom.min(axis=0) 
min2 = df3.tera.min(axis=0) 
df3min = min([min1,min2])
#maxes for plotting
max1 = df4.tbom.max(axis=0) 
max2 = df4.tera.max(axis=0) 
df4max = max([max1,max2])
min1 = df4.tbom.min(axis=0) 
min2 = df4.tera.min(axis=0) 
df4min = min([min1,min2])
#maxes for plotting
max1 = df5.tbom.max(axis=0) 
max2 = df5.tera.max(axis=0) 
df5max = max([max1,max2])
min1 = df5.tbom.min(axis=0) 
min2 = df5.tera.min(axis=0) 
df5min = min([min1,min2])
#maxes for plotting
max1 = df6.tbom.max(axis=0) 
max2 = df6.tera.max(axis=0) 
df6max = max([max1,max2])
min1 = df6.tbom.min(axis=0) 
min2 = df6.tera.min(axis=0) 
df6min = min([min1,min2])
#maxes for plotting
max1 = df7.tbom.max(axis=0) 
max2 = df7.tera.max(axis=0) 
df7max = max([max1,max2])
min1 = df7.tbom.min(axis=0) 
min2 = df7.tera.min(axis=0) 
df7min = min([min1,min2])
#maxes for plotting
max1 = df8.tbom.max(axis=0) 
max2 = df8.tera.max(axis=0) 
df8max = max([max1,max2])
min1 = df8.tbom.min(axis=0) 
min2 = df8.tera.min(axis=0) 
df8min = min([min1,min2])

fig = plt.figure(figsize=(6,12))

plt.subplot(4, 2, 1)
plt.scatter(df1.tbom, df1.tera, s=1,  color='0.3')
plt.plot([df1min, (df1max+2)], [df1min, (df1max+2)], 'k-', lw=2)
plt.title('Temperature 1000hPa')
plt.xlabel("T BoM (C)")
plt.ylabel("T Era Int (C)")

plt.subplot(4, 2, 2)
plt.scatter(df2.tbom, df2.tera, s=1,  color='0.3')
plt.plot([df2min, (df2max+2)], [df2min, (df2max+2)], 'k-', lw=2)
plt.title('Temperature 925hPa')
plt.xlabel("T BoM (C)")
plt.ylabel("T Era Int (C)")

plt.subplot(4, 2, 3)
plt.scatter(df3.tbom, df3.tera, s=1,  color='0.3')
plt.plot([df3min, (df3max+2)], [df3min, (df3max+2)], 'k-', lw=2)
plt.title('Temperature 850hPa')
plt.xlabel("T BoM (C)")
plt.ylabel("T Era Int (C)")

plt.subplot(4, 2, 4)
plt.scatter(df4.tbom, df4.tera, s=1,  color='0.3')
plt.plot([df4min, (df4max+2)], [df4min, (df4max+2)], 'k-', lw=2)
plt.title('Temperature 700hPa')
plt.xlabel("T BoM (C)")
plt.ylabel("T Era Int (C)")

plt.subplot(4, 2, 5)
plt.scatter(df5.tbom, df5.tera, s=1,  color='0.3')
plt.plot([df5min, (df5max+2)], [df5min, (df5max+2)], 'k-', lw=2)
plt.title('Temperature 500hPa')
plt.xlabel("T BoM (C)")
plt.ylabel("T Era Int (C)")

plt.subplot(4, 2, 6)
plt.scatter(df6.tbom, df6.tera, s=1,  color='0.3')
plt.plot([df6min, (df6max+2)], [df6min, (df6max+2)], 'k-', lw=2)
plt.title('Temperature 400hPa')
plt.xlabel("T BoM (C)")
plt.ylabel("T Era Int (C)")

plt.subplot(4, 2, 7)
plt.scatter(df7.tbom, df7.tera, s=1,  color='0.3')
plt.plot([df7min, (df7max+2)], [df7min, (df7max+2)], 'k-', lw=2)
plt.title('Temperature 300hPa')
plt.xlabel("T BoM (C)")
plt.ylabel("T Era Int (C)")

plt.subplot(4, 2, 8)
plt.scatter(df8.tbom, df8.tera, s=1,  color='0.3')
plt.plot([df8min, (df8max+2)], [df8min, (df8max+2)], 'k-', lw=2)
plt.title('Temperature 200hPa')
plt.xlabel("T BoM (C)")
plt.ylabel("T Era Int (C)")
plt.tight_layout()
plt.gcf().savefig(outpath+"T_scatter.png", dpi=400)