# now to iterate over all Era Int levels... using other .nc files in 
#/Users/bunnr/1_DATA/eraint_reanalysis/nc/

import numpy as np
from scipy import interpolate
from scipy.interpolate import interp1d
from nco import Nco  # for importing netCDF variables
import pandas as pd       # for reading csv files and  dataframes
nco = Nco()          # for calling nco instead of Nco
import glob
import time
import os.path
import os
from bisect import bisect_left, bisect_right


tic = time.clock()

# *********************************************
# input data
# *********************************************
stationid = "94866"
bomsitelat = -37.66
bomsitelon = 144.85

# getting era in levels
ifile = "/Users/bunnr/1_DATA/eraint_reanalysis/input_files/eragrid.nc"
lev = np.array(nco.ncks(input=ifile, returnArray="lev")) #extract Era Int Lev points over restricted domain
lev = lev.astype(np.float)
lev[:] = [x / 100 for x in lev] # convert from Pa to hPa
lev1 = lev

# create empty Era Interim arrays for later filling
tempera = np.zeros(len(lev), dtype = float)
tdera = np.zeros(len(lev), dtype = float)
rhera = np.zeros(len(lev), dtype = float)
thetaeera = np.zeros(len(lev), dtype = float)
# creating tbom, and tdbom arrays  for later filling (mix of temp and missing data)
tbom = np.empty(len(lev)) * np.nan 
tdbom = np.empty(len(lev)) * np.nan 
thetaebom =  np.zeros(len(lev))* np.nan 
# creating tdiff, tddiff, arrays  for later filling
tdiff = np.zeros(len(lev), dtype = float)
tddiff = np.zeros(len(lev), dtype = float)

#get Era Int Surrounding latons for bom site = YMML
df1 = pd.read_csv("/Users/bunnr/1_DATA/eraint_reanalysis/input_files/eraintlatlons"+stationid+".txt", 
                  names=["lon","lat"], header = None, dtype=float)
xsurrpoints = df1['lon'].values.tolist()
ysurrpoints = df1['lat'].values.tolist()
del df1

#get Era Int Surrounding latlon indicies for bom site = YMML
df2 = pd.read_csv("/Users/bunnr/1_DATA/eraint_reanalysis/input_files/eraintindices"+stationid+".txt")
x0 = int(df2.eralonidx[0])
x1 = int(df2.eralonidx[1])
y0 = int(df2.eralatidx[0])
y1 = int(df2.eralatidx[1])
del df2

# *********************************************
# Td function
# *********************************************
# RH to Td calculation approximation valid for -40 degC < T < 50 degC, 1% < RH < 100%
# Constants for Td calculation are from  Alduchov and Eskridge (1996) :
a = 17.625
b = 243.04 # degC 
def acc_dewpoint_approximation(T,RH):
    Td = b*(acc_gamma(T,RH) + (a*T/(b+T)))/(a - acc_gamma(T,RH) - (a*T/(b+T)))
    return Td 
def acc_gamma(T,RH): 
    g = np.log(RH/100.0) 
    return g
# *********************************************
# bounding function for Bom T, Td interpolation
# *********************************************
def within(p, q, r):
    "Return true iff q is between p and r (inclusive)."
    #return p <= q <= r or r <= q <= p
    return r <= q < p

# *********************************************
# file paths
# *********************************************
# get BoM sounding file names for matching
bomfnames = glob.glob("/Users/bunnr/1_DATA/bomsounding/"+stationid+"/sounding*")

#temp file to delimit bom sounding files
ftmp1 = "/Users/bunnr/1_DATA/bomsounding/ftmp1.txt"
ftmp2 = "/Users/bunnr/1_DATA/bomsounding/ftmp2.txt"
outpath = "/Users/bunnr/2_OUTPUT/stable_layer/era_bomsounding_comparison/dataframes/"


# *********************************************
# Empty tdiff and tddiff dataframe for all soundings
# *********************************************

index = [i for i in xrange(len(bomfnames))]
bits1 = np.chararray(4, itemsize=16)
bits2 = np.array(lev, dtype=str)
bits1[0] = "datetime"
bits1[1] = "stationid"
bits1[2] = "BoM inversion"
bits1[3] = "EraInt inversion"
columns = np.concatenate((bits1, bits2)) 
dftdiff = pd.DataFrame(index = index, columns=columns)
dftddiff = pd.DataFrame(index = index, columns=columns)


# calculate tbom, tdbom, tera, tdera by matching each bom trace to corresponding era netcdf
# NOTE - bom file names MUST be of the following structure ...YYYY-MM-DDHH.txt
# NOTE - era file names MUST be of the following structure ...YYYYMMDD_HH.nc

nummatched0 = 0 
nummatched = 0
for i in range(len(bomfnames)):
    getdatetime = str(bomfnames[i])
    yr = getdatetime[-16:-12]
    mt = getdatetime[-11:-9]
    dy = getdatetime[-8:-6]
    hr = getdatetime[-6:-4]
    
    

    fname = "/Users/bunnr/1_DATA/eraint_reanalysis/nc/upper/zoomed_94866/T_RH_"+yr+mt+dy+"_"+hr+".nc"
    if os.path.isfile(fname) == True:
        sounding = str(bomfnames[i])
        print sounding
        # for each sounding and matching era file do...
        
        # *********************************************
        # get the Era Int temperature (var130), Dewpoint 
        # (var157) variables over the restricted domain 
        # *********************************************
       
        temp = np.array(nco.ncks(input=fname, returnArray="var130")) #extract Era Int Temp points over restricted domain
        temp = temp.astype(np.float)
        rh = np.array(nco.ncks(input=fname, returnArray="var157")) #extract Era Int RH points over restricted domain
        rh = rh.astype(np.float)        
        # for all Era Int levels, interpolate linearly to BoM site latlon location
        # based on 4 surrounding points era int points (prefiously defined for site YMML)
        for i in range(len(lev)):
            # create Era Int Temerature array
            tsurr= np.array([temp[0,i,x0,y0],temp[0,i,x1,y0],temp[0,i,x0,y1],temp[0,i,x1,y1]]).reshape(2,2)
            f3 = interpolate.interp2d(xsurrpoints, ysurrpoints, tsurr, kind='linear')
            tempera[i] = f3(bomsitelon,bomsitelat) - 273.15
    
            # create Era Int Dew Point Temperature array, based on Era Int RH
            rhsurr= np.array([rh[0,i,x0,y0],rh[0,i,x1,y0],rh[0,i,x0,y1],rh[0,i,x1,y1]]).reshape(2,2)
            f4 = interpolate.interp2d(xsurrpoints, ysurrpoints, rhsurr, kind='linear')
            rhera[i] = f4(bomsitelon,bomsitelat)
            tdera[i] = acc_dewpoint_approximation(tempera[i],rhera[i])
            
            del f3, f4
        del temp, rh
        # *********************************************
        # getting Bom sounding T and Td data
        # *********************************************
        df = pd.read_csv(sounding)
        df.columns = ["col1"]
        strdate = str(df.col1[3])
        df.col1.map(len).max()                                           # get max row length
        df = df[(df.col1.map(len) == df.col1.map(len).max())]            # retain only rows with max length
        df = df.set_index(np.array(range(len(df))))                      # relist rows 0...N
        header = str(df.col1[1]).split()                                 # get header
        df = df.drop(df.index[[0,1,2,3]])                                # get rid of header
        df = df.set_index(np.array(range(len(df))))                      # relist rows 0...N
        #df = pd.DataFrame(df.col1.str.split().tolist(), columns=header)  # split col1 string, & add strings to df
        # can't split header at end of word retaining leading whitespace.... this almost works 
        # re.split("\s(?!\w{1})", str(df.col1[1])), but gives " ", " ", " PRES", NOT "   PRES"
        # getting number of columns
        ncols = len(str(df.col1[1]).split())                             # assuming even width (which they are in this case)
        colwidth = int(df.col1.map(len).max()/ncols)                     # get colwidth
        # write temperary file for later reading, with commas inserted at end of each column width
        df.to_csv(ftmp1, header=None, index=False)  
        fin = open(ftmp1, 'r')
        fout = open(ftmp2, 'w')
        for line in fin:
            #newline = ','.join([line[x:x+colwidth] for x in range(0, len(line),colwidth)])
            newline = ','.join([line[x:x+7] for x in range(0, len(line),7)])
            fout.write(newline)
        fin.close()
        fout.close()
        df = pd.read_csv(ftmp2, ",", names = header, index_col=False)    # reading in the ftmp2 file now comma delimitted
        df = df.applymap(lambda x: np.nan if isinstance(x, basestring) and x.isspace() else x)   # replace whitespace with NaN
        
        #os.remove(ftmp1)
        #os.remove(ftmp2)
        
        # *********************************************
        # getting Tbom, Tdbom values at Era Int Levels
        # *********************************************
        # convert df column to 1d array for interpolation
        P = np.array(df.PRES.values, dtype = float)
        T = np.array(df.TEMP.values, dtype = float)
        TD = np.array(df.DWPT.values, dtype = float)
        del df

        # Source level grid for interp/extrap = BoM Pressure
        source = P
        
        # Target level grid to obtain BoM T, Td values for  = Era Int lev
        target = lev

        for i in range(len(source)-1):
            for j in range(len(target)-1):
                if (i <= (len(source)-1)):
                    if (within(source[i], target[j], source[i+1])==True):
                        f1 = interp1d([source[i],source[i+1]], [T[i],T[i+1]], kind='linear')
                        f2 = interp1d([source[i],source[i+1]], [TD[i],TD[i+1]], kind='linear')
                        tbom[j] = f1(target[j])
                        tdbom[j] = f2(target[j])
                        del f1, f2
                else:
                    if (within(source[i-1], target[j], source[i])==True):
                        f1 = interp1d([source[i-1],source[i]], [T[i-1],T[i]], kind='linear')
                        f2 = interp1d([source[i],source[i+1]], [TD[i],TD[i+1]], kind='linear')
                        tbom[j] = f1(target[j])
                        tdbom[j] = f2(target[j])
                        del f1, f2
                            

        del P, T, TD, target, source
        

        
        # *********************************************
        # getting Tdiff, Tddiff arrays at Era Int Levels
        # *********************************************

        for i in range(len(lev)):
            if (np.isreal(tbom[i])==True)&(np.isreal(tempera[i])==True):
                tdiff[i] = tbom[i] - tempera[i]
            if (np.isreal(tdbom[i])==True)&(np.isreal(tdera[i])==True):
                tddiff[i] = tdbom[i] - tdera[i]
        
        tdiff = np.array(tdiff, dtype=object)     # conversion to np array for df insertion
        tddiff = np.array(tddiff, dtype=object)   # conversion to np array for df insertion
        
        
        # *********************************************
        # creating Tdiff, Tddiff dataframe 
        # *********************************************
        # Define string format for datetime conversion, and empty lists to be filled.
        format = "%Y%m%d%H"
        datetime = pd.to_datetime(str(yr).zfill(2)+str(mt).zfill(2)+\
                         str(dy).zfill(2)+str(hr).zfill(2), format=format)

        
        data1 = np.concatenate(([datetime, stationid, "NaN" , "NaN" ], tdiff))
        data2 = np.concatenate(([datetime, stationid, "NaN" , "NaN" ], tddiff))

        #print "nummatched", nummatched

          
        dftdiff.loc[nummatched] = np.array(data1)
        dftddiff.loc[nummatched] = np.array(data2)

        nummatched=nummatched0 + 1   # count for number of matched era and bom files
        nummatched0 = nummatched

del nummatched, tdiff, tddiff

# SAVING TDIFF Dataframe to file
dftdiff.to_csv(outpath+"tdiff.csv" ,header = "true")
# SAVING TDDIFF Dataframe to file
dftddiff.to_csv(outpath+"tddiff.csv" ,header = "true")

toc = time.clock()
print toc - tic
