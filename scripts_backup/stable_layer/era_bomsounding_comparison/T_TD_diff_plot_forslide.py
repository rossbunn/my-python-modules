# plottsing Tdiff, TDdiff - for slide

import numpy as np
from bisect import bisect_left, bisect_right
import matplotlib.pyplot as plt
import pandas as pd
import math

%matplotlib inline

inpath = "/Users/bunnr/2_OUTPUT/stable_layer/era_bomsounding_comparison/dataframes/"
outpath = "/Users/bunnr/2_OUTPUT/stable_layer/era_bomsounding_comparison/plots/"

#getting summary stats
dftdiff = pd.read_csv(inpath+"tdiffsummary_new.csv" , index_col=False)
dftddiff = pd.read_csv(inpath+"tddiffsummary_restricted.csv" , index_col=False)


dftdiff = dftdiff.reindex(index=dftdiff.index[::-1])
dftddiff = dftddiff.reindex(index=dftddiff.index[::-1])

labelsize = 12
labelcolor = "0.3"
titlesize = 16
titlecolor = "k"

fig = plt.figure(figsize=(3,6))

plt.subplot(2,1, 1)
#for line and dot plotting
plt.plot(dftdiff.Tdiff_mean, dftdiff.Era_Int_P, 'r.-', lw = 2,  ms=12)
ax = plt.gca()
ax.set_ylim(ax.get_ylim()[::-1])
ax.set_xlim([-3, 3])
for tick in ax.xaxis.get_major_ticks():
    tick.label1.set_fontsize(labelsize)
    tick.label1.set_fontweight('bold')
    tick.label1.set_color(labelcolor)
for tick in ax.yaxis.get_major_ticks():
    tick.label1.set_fontsize(labelsize)
    tick.label1.set_fontweight('bold')
    tick.label1.set_color(labelcolor)
plt.plot([0, 0], [0, 1000], 'k-', lw=2)
plt.ylabel("Pressure (hPa)", weight = 'bold', fontsize = labelsize, color = labelcolor)
plt.xlabel("T (degrees Celcius)", weight = 'bold', fontsize = labelsize, color = labelcolor)
plt.title("Mean T Difference \n(Tbom - Tera)", y=1.0, weight = 'bold', fontsize = titlesize, color=titlecolor)

plt.subplot(2,1, 2)
plt.plot(dftddiff.Tddiff_mean, dftddiff.Era_Int_P,'b.-', ms=12, lw=2)
ax = plt.gca()
ax.set_ylim(ax.get_ylim()[::-1])
#ax.set_xlim([-2, 2])
for tick in ax.xaxis.get_major_ticks():
    tick.label1.set_fontsize(labelsize)
    tick.label1.set_fontweight('bold')
    tick.label1.set_color(labelcolor)
for tick in ax.yaxis.get_major_ticks():
    tick.label1.set_fontsize(labelsize)
    tick.label1.set_fontweight('bold')
    tick.label1.set_color(labelcolor)
plt.plot([0, 0], [0, 1000], 'k-', lw=2)
plt.ylabel("Pressure (hPa)", weight = 'bold', fontsize = labelsize, color = labelcolor)
plt.xlabel("TD (degrees Celcius)", weight = 'bold', fontsize = labelsize, color = labelcolor)
plt.title("Mean TD Difference \n(Tbom - Tera)", y=1.0, weight = 'bold', fontsize = titlesize, color=titlecolor)

fig.tight_layout()
plt.gcf().savefig(outpath+"T_TD_DIFF_forslide.png", dpi=400)

plt.show()

