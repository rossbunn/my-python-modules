# Re-plotting Tdiff, TDdiff Giles 00z / Melb 00Z & 12Z  - with Tdiff_mean and TDdiff_mean sign reversed

import numpy as np
from bisect import bisect_left, bisect_right
import matplotlib.pyplot as plt
import pandas as pd

%matplotlib inline

# *********************************************
# input data - Melbourne
# *********************************************
statname = "Melbourne"
stationid = "94866"
bomsitelat = -37.66
bomsitelon = 144.85
hour = "00"
# hour = "12"

# *********************************************
# input data - Giles
# *********************************************
# statname = "Giles"
# stationid = "94461"
# bomsitelat = -25.03
# bomsitelon = 128.28
# hour = "00"


inpath = "/Users/bunnr/2_OUTPUT/stable_layer/era_bomsounding_comparison/dataframes/"
outpath = "/Users/bunnr/2_OUTPUT/stable_layer/era_bomsounding_comparison/plots/"
erainpath = "/Users/bunnr/1_DATA/eraint_reanalysis/nc/upper/"

#getting summary stats
dftdiff = pd.read_csv(inpath+"tdiff_summary_"+stationid+"_"+hour+".txt", index_col=False)
# choose mean, var data which was calculated with lots of data (some levels have <10 T, TD values)
dftdiff = dftdiff[dftdiff.n > 100]
dftdiff = dftdiff.set_index(np.array(range(len(dftdiff))))    # relist from 0 ... N
dftdiff['Tdiff_mean'] *= -1                                   # reverse the sign to make it Tera-Tbom

dftddiff = pd.read_csv(inpath+"tddiff_summary_"+stationid+"_"+hour+".txt", index_col=False)
# choose mean, var data which was calculated with lots of data (some levels have <10 T, TD values)
dftddiff = dftddiff[dftddiff.n > 100]
dftddiff = dftddiff.set_index(np.array(range(len(dftddiff)))) # relist from 0 ... N
dftddiff['Tddiff_mean'] *= -1                                 # reverse the sign to make it Tdera-Tdbom

dftdiff = dftdiff.reindex(index=dftdiff.index[::-1])
dftddiff = dftddiff.reindex(index=dftddiff.index[::-1])
lev = dftdiff.Era_Int_P

# create STD from VAR
dftdiff_STD = (dftdiff.Tdiff_VAR)**0.5
dftddiff_STD = (dftddiff.Tddiff_VAR)**0.5

fig = plt.figure(figsize=(6,12))

plt.subplot(4, 2, 1)
#for line and dot plotting
for i in range (len(lev)):
#    plt.plot([(dftdiff.Tdiff_mean[i] - dftdiff.Tdiff_VAR[i]), (dftdiff.Tdiff_mean[i] + dftdiff.Tdiff_VAR[i]) ], 
#             [lev[i], lev[i]], linestyle='-', color="#808080", lw = 4,  ms=8, hold = True)
    plt.plot([(dftdiff.Tdiff_mean[i] - dftdiff_STD[i]), (dftdiff.Tdiff_mean[i] + dftdiff_STD[i]) ], 
             [lev[i], lev[i]], linestyle='-', color="#808080", lw = 4,  ms=8, hold = True)
plt.plot(dftdiff.Tdiff_mean, lev, 'r.-', lw = 2,  ms=8)
# for dot plotting
#plt.plot(dftdiff.Tdiff_mean, lev, 'r.', ms=5)
ax = plt.gca()
ax.set_ylim(ax.get_ylim()[::-1])
ax.set_xlim([-3, 3])
plt.plot([0, 0], [0, 1000], 'k-', lw=2, ms=5)
plt.ylabel("Pressure (hPa)")
plt.xlabel("Mean Temperature Difference \n(degrees Celcius)")
plt.title("2013 "+statname+" "+hour+" UTC"+"\nMean T Difference \n(Tera - Tbom)\n + Era Warm Bias\n - Era Cool Bias\n Grey +- sigma", 
          y=1.0, weight = 'bold')

plt.subplot(4, 2, 2)
for i in range (len(lev)):
#    plt.plot([(dftdiff.Tdiff_mean[i] - dftdiff.Tdiff_VAR[i]), (dftdiff.Tdiff_mean[i] + dftdiff.Tdiff_VAR[i]) ], 
#             [lev[i], lev[i]], linestyle='-', color="#808080", lw = 4,  ms=8, hold = True)
    plt.plot([(dftddiff.Tddiff_mean[i] - dftddiff_STD[i]), (dftddiff.Tddiff_mean[i] + dftddiff_STD[i]) ], 
             [lev[i], lev[i]], linestyle='-', color="#808080", lw = 4,  ms=8, hold = True)
plt.plot(dftddiff.Tddiff_mean, lev,'b.-', lw=2, ms=8)
ax = plt.gca()
ax.set_ylim(ax.get_ylim()[::-1])
plt.plot([0, 0], [0, 1000], 'k-', lw=2)
plt.ylabel("Pressure (hPa)")
plt.xlabel("Mean Dew Point Difference \n(degrees Celcius)")
plt.title("2013 "+statname+" "+hour+" UTC"+"\nMean TD Difference \n(TDera - TDbom)\n + Era Moist Bias\n - Era Dry Bias\n Grey +- sigma", 
          y=1.0, weight = 'bold')

plt.subplot(4, 2, 3)
#for line and open dot  plotting
#plt.plot(dftdiff.Tdiff_STD, lev,'.', linestyle='-', color="#FF4500", lw = 1, markerfacecolor='None', ms=5)
# for line dot plotting
plt.plot((dftdiff.Tdiff_VAR)**0.5, lev,'.', linestyle='-', color="#FF4500", lw = 2, ms=8)
# for dot plotting
#plt.plot(dftdiff.Tdiff_STD, lev,'.', color="#FF4500", ms=5)
ax = plt.gca()
ax.set_ylim(ax.get_ylim()[::-1])
plt.ylabel("Pressure (hPa)")
plt.xlabel("Standard Deviation \n Temperature Difference \n(degrees Celcius)")
plt.title("STD T Difference \n(Tera - Tbom)", y=1.0, weight = 'bold')

plt.subplot(4, 2, 4)
plt.plot((dftddiff.Tddiff_VAR)**0.5, lev, 'c.-', lw = 2, ms=8)
ax = plt.gca()
ax.set_ylim(ax.get_ylim()[::-1])
plt.ylabel("Pressure (hPa)")
plt.xlabel("Standard Deviation \n Dew Point Difference \n(degrees Celcius)")
plt.title("STD TD Difference \n(TDera - TDbom)", y=1.0, weight = 'bold')


fig.tight_layout()
plt.gcf().savefig(outpath+"T_TD_DIFF_STD_"+statname+"_"+hour+".png", dpi=200)

plt.show()

