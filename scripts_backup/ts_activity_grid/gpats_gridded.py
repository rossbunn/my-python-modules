#!/usr/bin/env python
""" Probably can only process one year at a time due to memory issues.
    NOTE - this script needs the input data to be concatenated to one large file on the cmd line:
    1. $ which -a awk , gives /usr/bin/awk for my mac
    2. edit .dates.awk top line to now be #!/usr/bin/awk -f
    3. cat /Users/bunnr/1_DATA/gpats/gpats_full/2011*.txt | ./dates.awk > full_processed_2011.txt  
    
    Script takes year (integer YYYY format), YYYY GPATS lightning stroke location data with
    processed datetimes, time period options, and on Era Interim grid (or user defined), counts number of 
    cloud to ground, ground to cloud and intra cloud strokes. Strokecounts per time period 
    are copied to netCDF files. Option to count GPATS within box of specific sites.
"""

import glob
import os
import datetime as dt
import sys
import numpy as np
import pandas as pd
from pylab import *
import numpy.ma as ma
import matplotlib.pyplot as plt  # to plot anything
import cartopy.crs as ccrs       # for map coastlines plotting
import shapely.geometry as sgeom # for plotting boxes
from nco import Nco
nco = Nco()
import netCDF4
import time
# to be able to plot inline within ipython notebook
# %matplotlib inline
# enable interactivity
# plt.ioff() 


yr = 2008
test=False
plot=False

    
def main(yr, test, plot):
    # dates to count strikes - defined by dates where GPATS data is available:
    # IF yr = 2008, then startdate = dt.date(2008,3,1)
    # IF yr = 2014, then   enddate = dt.date(2014,10,31)
    # and reliable 
    # (post Jan 2007 see Kuleshov et.al. 2011 "Occurrence of positive and negative polarity cloud-to-ground 
    # lightning flashes: Case study of CGR4 and GPATS data for Brisbane, Australia")

    
    if yr ==2008: 
        startdate = dt.date(yr,3,1)
    else:
        startdate = dt.date(yr,1,1)
    if yr ==2014: 
        enddate = dt.date(yr,10,31)
    else:
        enddate = dt.date(yr,12,31)
        
    if test ==True:
        # test point GPATS +-1hr 11Z sounding time, +-0.25deg
        # (26/02/2012 11:00 MELBOURNE AIRPORT 86282 144.8321 -37.6655 94866)
#         fname = "test2012.txt"
        fname = "test201202full.txt"
        path = "/Users/bunnr/2_OUTPUT/lightning/ts_activity_grid/test"
        opath="/Users/bunnr/2_OUTPUT/lightning/ts_activity_grid/netcdf"
#         gridtype="test"
        gridtype="era"
        startdate = dt.date(yr,2,1)
        enddate = dt.date(yr,2,1)
    else:
        fname = "full_processed_"+str(yr)+".txt"
        path= opath= "/Users/bunnr/2_OUTPUT/lightning/ts_activity_grid/netcdf"
        gridtype="era"
    
#     all_times=range(24)
    all_times=range(1,24) + [0]
        
    # Load in all the CSV data.
    gpats_df = load_and_process_data(path,fname)
    print "Data load complete!"
  
    d = startdate
    delta = dt.timedelta(days=1)
    while d <= enddate:
        oname = "lightning_eragrid_Australia_"+str(d)+".nc"
        this_date = str(d)
        
        # get grid specifications
        LON_START,LON_END,LON_STEP,LAT_START,LAT_END,LAT_STEP,lon_data,lat_data = gridspecs(gridtype=gridtype)
        # create arrays for filling
        cloud_ground_neg_all = np.empty([24,len(lat_data),len(lon_data)],dtype=float)
        cloud_ground_pos_all = np.empty([24,len(lat_data),len(lon_data)],dtype=float)
        intra_cloud_all = np.empty([24,len(lat_data),len(lon_data)],dtype=float)
        total_lightning_all = np.empty([24,len(lat_data),len(lon_data)],dtype=float)
        
        cloud_ground_neg = gpats_df[(gpats_df.index.date== d) &(gpats_df["Polarity"]<0.0)]
        cloud_ground_pos = gpats_df[(gpats_df.index.date== d) &(gpats_df["Polarity"]>0.0)]
        intra_cloud = gpats_df[(gpats_df.index.date== d) &(gpats_df["Polarity"]==0.0)]
        
        cg_daily,gc_daily,ic_daily,tot_daily =daily_grids(opath,oname,
                    cloud_ground_neg,cloud_ground_pos,intra_cloud,
                    LON_START,LON_END,LON_STEP,LAT_START,LAT_END,LAT_STEP,lon_data,lat_data,
                    cloud_ground_neg_all,cloud_ground_pos_all,intra_cloud_all,total_lightning_all,
                    this_date = this_date,yr=str(yr),test=test,timetype=3,plot=plot)
    
        create(opath,oname,this_date,lon_data,lat_data,all_times,cg_daily,gc_daily,ic_daily,tot_daily)
        
        d += delta  

def daily_grids(opath,oname,cloud_ground_neg,cloud_ground_pos,intra_cloud,
                LON_START,LON_END,LON_STEP,LAT_START,LAT_END,LAT_STEP,lon_data,lat_data,
                cloud_ground_neg_all,cloud_ground_pos_all,intra_cloud_all,total_lightning_all,
                this_date = "2012-02-01",yr=str(yr),test=False, timetype=3, plot=False):
    """ Create a pandas dataframe with the GPATS lightning strike activity for 1 year, 
        based on user defined year, sites, time types:
        
           timetype = 1 (Australian sounding times, -1 hour to create time period )
           timetype = 2 (ECMWF Era Interim times, -1 hour to create time period )
           timetype = 3 (hourly time periods for full day) 

    """
    assert isinstance(yr, basestring), "yr must be a string"
    assert len(yr)==4, "yr must have length of 4 ie 2012 not 12"
    assert timetype <= 3 and timetype >=0, "timetype must be 1, 2 or 3"
        
    if timetype == 1:
        # sounding times
        TIMES = [("08:00", "09:00"), ("11:00", "12:00"),
                 ("20:00", "21:00"), ("23:00", "00:00")]
    elif timetype==2:
        # ECMWF Era Interim times
        TIMES = [("23:00", "00:00"), ("05:00", "06:00"),
                 ("11:00", "12:00"), ("17:00", "18:00")] 
    elif timetype==3:
        # all hours per day
        a = range(24) 
        b = range(1,24) + [0]
        c = [str(x)+":00" for x in a]
        d = [str(x)+":00" for x in b]
        TIMES = zip(c,d)


    # Make the numpy histogram array
    lon_vals = np.arange(LON_START-LON_STEP, LON_END+LON_STEP, step=LON_STEP)
    lat_vals = np.arange(LAT_START-LAT_STEP, LAT_END+LAT_STEP, step=LAT_STEP)
#     print "lon_vals",len(lon_vals),lon_vals[0],lon_vals[-1],LON_START,LON_END,LON_STEP
#     print "lat_vals",len(lat_vals),lat_vals[0],lat_vals[-1],LAT_START,LAT_END,LAT_STEP
    
    i=0
    for time in TIMES:
        
#         print "doing for full grid at times "+str(time[0])+" to "+str(time[1])
        # Filter all strikes everywhere by time period
#         print "cloud_ground_neg",cloud_ground_neg
        this_time_neg = cloud_ground_neg.between_time(time[0], time[1])
        this_time_pos = cloud_ground_pos.between_time(time[0], time[1])
        this_time_zero = intra_cloud.between_time(time[0], time[1])
                
        hist_cloud_ground_neg,cg_gridy,cg_gridx = np.histogram2d(this_time_neg["Lat"].values,
                                     this_time_neg["Lon"].values,
                                     bins=[lat_vals, lon_vals])
        hist_cloud_ground_pos,gc_gridy,gc_gridx  = np.histogram2d(this_time_pos["Lat"].values,
                                     this_time_pos["Lon"].values,
                                     bins=[lat_vals, lon_vals])
        hist_intra_cloud,ic_gridy,ic_gridx  = np.histogram2d(this_time_zero["Lat"].values,
                                     this_time_zero["Lon"].values,
                                     bins=[lat_vals, lon_vals])
#         print "cg hist shape", hist_cloud_ground_neg.shape
#         print "gc hist shape",hist_cloud_ground_pos.shape
#         print "ic hist shape",hist_intra_cloud.shape
        
#         print "hist_cloud_ground_neg",hist_cloud_ground_neg.any()>0.0
#         print "hist_cloud_ground_pos",hist_cloud_ground_pos.any()>0.0
#         print "hist_intra_cloud",hist_intra_cloud.any()>0.0
        
        hist_cloud_ground_neg=np.rot90(hist_cloud_ground_neg.T)
        hist_cloud_ground_pos=np.rot90(hist_cloud_ground_pos.T)
        hist_intra_cloud=np.rot90(hist_intra_cloud.T)
        hist_total_lightning = hist_cloud_ground_neg + hist_cloud_ground_pos + hist_intra_cloud
#         print "tot max",np.amax(hist_total_lightning)
#         print "hist_total_lightning shape", hist_total_lightning.shape
        
        # fill arrays
        cloud_ground_neg_all[i,:,:] = hist_cloud_ground_neg
        cloud_ground_pos_all[i,:,:] = hist_cloud_ground_pos
        intra_cloud_all[i,:,:] = hist_intra_cloud
        total_lightning_all[i,:,:] = hist_total_lightning
        
        if (test==True)&(i==7):
#             np.savetxt(os.path.join(path,"cg_"+str(this_date)+"_"+str(i)+".txt"), cloud_ground_neg_all[i,:,:], fmt='%i', delimiter = ",")
#             np.savetxt(os.path.join(path,"gc_"+str(this_date)+"_"+str(i)+".txt"), cloud_ground_pos_all[i,:,:], fmt='%i', delimiter = ",")
#             np.savetxt(os.path.join(path,"ic_"+str(this_date)+"_"+str(i)+".txt"), intra_cloud_all[i,:,:], fmt='%i', delimiter = ",")
#             np.savetxt(os.path.join(path,"tt_"+str(this_date)+"_"+str(i)+".txt"), total_lightning_all[i,:,:], fmt='%i', delimiter = ",")
            strikes="on"
            if strikes=="on":
                gptot = this_time_pos.append(this_time_neg)
                gptot = gptot.append(this_time_zero)
                gptot =gptot.set_index(np.array(range(len(gptot)))) # relist from 0... len(df)
                gp_lon=gptot["Lon"]
                gp_lat=gptot["Lat"]
            plotpath = path
            plotname = "tt_"+str(this_date)+"_"+str(i)+".png"
            title = "total lightning for "+str(this_date)+" at "+str(i)+"UTC"
            data = hist_total_lightning
#             vmax="max of data"
            cmap="ocean_r"
            vmax=4
#             cmap="binary"
            showplot(plotpath,plotname,title,cg_gridx,cg_gridy,
                     data,lon_vals,lat_vals,cmap=cmap,vmax=vmax,strikes=strikes,
                     gp_lon=gp_lon,gp_lat=gp_lat)
            
        if (plot==True)&(i==23):
            strikes="on"
            if strikes=="on":
                gptot = this_time_pos.append(this_time_neg)
                gptot = gptot.append(this_time_zero)
                gptot =gptot.set_index(np.array(range(len(gptot)))) # relist from 0... len(df)
                gp_lon=gptot["Lon"]
                gp_lat=gptot["Lat"]
            plotpath = path
            plotname = "tt_"+str(this_date)+"_"+str(i)+".png"
            title = "total lightning for "+str(this_date)+" at "+str(i)+"UTC"
            data = hist_total_lightning
            vmax="max of data"
            showplot(plotpath,plotname,title,cg_gridx,cg_gridy,
                     data,lon_data,lat_data,cmap='binary',vmax=vmax,strikes="on",
                     gp_lon=gp_lon,gp_lat=gp_lat)
    
        i=i+1
    return cloud_ground_neg_all,cloud_ground_pos_all,intra_cloud_all,total_lightning_all
    
def gridspecs(gridtype="era",ipath="/Users/bunnr/1_DATA/eraint_reanalysis/nc/upper",iname="T_RH_GH_20080101_00.nc"):
    """ Defines grid specifications for GPATS strokes to be counted within. Counts are within boxes
        centered on each grid point.
        
        gridtype = "test" (small grid at Era Interim spacing over Melbourne )
        gridtype = "era"  (ECMWF Era Interim grid over Australia)
        gridtype = "user" (user defined grid - MUST BE EQUAL LAT AND LON SPACING) 
    """
    if gridtype=="test":
        LAT_STEP = LON_STEP = 0.25
        LON_START = 144.8321-(LON_STEP*5.0)
        LON_END = 144.8321+(LON_STEP*5.0)
        LAT_START = -37.6655-(LAT_STEP*5.0)
        LAT_END = -37.6655+(LAT_STEP*5.0)
        lon_data = np.arange(LON_START, LON_END, step=LON_STEP) #original lons before +1 deg space added
        lat_data = np.arange(LAT_START, LAT_END, step=LAT_STEP) #original lats before +1 deg space added
        
    if gridtype=="era":
        # Era Interim Specs from:
        # ncdump -v lat /Users/bunnr/1_DATA/eraint_reanalysis/nc/upper/T_RH_GH_20141026_12.nc
        # ncdump -v lon /Users/bunnr/1_DATA/eraint_reanalysis/nc/upper/T_RH_GH_20141026_12.nc
        # Create the grid to bin the number of stokes. 
        # Grid domain is over tropics, Tasma, India Oceans. Over ACCESS A domain -55 to 4.73, 95 to 169.69
        
        ifile = os.path.join(ipath,iname)
        lon_data = np.array(nco.ncks(input=ifile, returnArray="lon")) #extract Era Int Lat points over Australia
        lon_data = lon_data.astype(np.float)
        lat_data = np.array(nco.ncks(input=ifile, returnArray="lat")) #extract Era Int Lon points over Australia
        lat_data = lat_data.astype(np.float)
        
#         LON_STEP = 0.703125
#         LON_START= 109.688
#         LON_END  = 160.313
#         LAT_STEP = 0.701753
#         LAT_START= -45.263048035017
#         LAT_END  = -9.47366604563595
        LON_STEP = abs(lon_data[1]-lon_data[0])
        LON_START= lon_data.min()
        LON_END  = lon_data.max()
        LAT_STEP = abs(lat_data[1]-lat_data[0])
        LAT_START= lat_data.min()
        LAT_END  = lat_data.max()

    if gridtype=="user":
        LON_START = 112.0
        LON_END = 156.3
        LAT_START = -44.5
        LAT_END = -9.95
        LAT_STEP = LON_STEP = 0.05
        lon_data = np.arange(LON_START, LON_END, step=LON_STEP) #original lons before +1 deg space added
        lat_data = np.arange(LAT_START, LAT_END, step=LAT_STEP) #original lats before +1 deg space added

        
    return LON_START,LON_END,LON_STEP,LAT_START,LAT_END,LAT_STEP,lon_data,lat_data
    
def load_and_process_data(path,fname):
    """ Load in all the gpats data and add a time index for soundings. """
    print "loading data", os.path.join(path, fname)
#     path = os.getcwd()
    ingpats = glob.glob(os.path.join(path, fname))
    print ingpats

    col_names = ["timestamp", "Lat", "Lon", "Polarity"]

    dfgptotal = pd.read_csv(ingpats[0], sep=r",", header=None,
                            names=col_names, parse_dates=True,
                            index_col=0)

    return dfgptotal


def find_grid_points(lats, lons,LON_NUM,LON_START,LON_RANGE,LAT_NUM,LAT_START,LAT_RANGE):
    """ For a given column of lons and lats find what grid points they should fit into.

    This should be double-checked to make sure it gets the correct coordinates!

    """

    lon_coords = np.floor(LON_NUM * (lons - LON_START) / LON_RANGE).astype(int)
    lat_coords = np.floor(LAT_NUM * (lats - LAT_START) / LAT_RANGE).astype(int)

    return (lat_coords, lon_coords)

def showplot(fpath,fname,title,lon_hist,lat_hist,hist,lon_data,lat_data,cmap='binary',vmax="max of data",strikes="off",gp_lon=[],gp_lat=[]):
    lonstep=abs(lon_data[1]-lon_data[0])
    latstep=abs(lat_data[1]-lat_data[0])
    lat_hist2 =np.flipud(lat_hist)
#     print "lat hist", lat_hist
#     print "lat hist2", lat_hist2
#     print "lat data", lat_data
    
    box = sgeom.box(minx=min(lon_data), maxx=max(lon_data), miny=min(lat_data), maxy=max(lat_data))
#     box = sgeom.box(minx=142.0, maxx=148.0, miny=-34.0, maxy=-40.0) # for testing
    x0, y0, x1, y1 = box.bounds
    proj = ccrs.PlateCarree()
    fig = plt.figure(figsize=(20,20))
    ax = plt.axes(projection=proj)
    ax.set_xticks(np.arange(lon_data.min(),lon_data.max(),lonstep))
    ax.set_yticks(np.arange(lat_data.min(),lat_data.max(),latstep))
    ax.set_extent([x0, x1, y0, y1], proj)
    if strikes =="on":
        plt.plot(gp_lon, gp_lat, 'r+', ms=8, mew=1)
    if vmax != "max of data":
        plt.pcolormesh(lon_hist, lat_hist2, hist, cmap=cmap, vmax=vmax)
    else:
        plt.pcolormesh(lon_hist, lat_hist2, hist, cmap=cmap)
    ax.coastlines()
    plt.colorbar()
    plt.grid(True)
    plt.title(title)
#     plt.show()
    plt.gcf().savefig(os.path.join(fpath,fname), dpi=200)
    print "image saved at "+ os.path.join(fpath,fname)


def create(opath,oname,datestamp,lon_data,lat_data,all_times,cg_daily,gc_daily,ic_daily,tot_daily):
    """ Script creates a new netCDF using existing Era Interim or user defined grid, 
        time and data.
    """

    filenotes = 'This file was created by Ross Bunn using gpatsgridded.py. Global Position and Tracking System or GPATS is a commercial lightning detection system, using ground-based Very Low Frequency sensors across Australia to accurately detect stroke times, and Time Difference of Arrival methods to calculate locations. On the ECMWF Era Interim Grid (lon res=0.703125deg, lat res= 0.701753deg), (or user defined grid) over an Australian domain (-45.263 to -9.473, 109.688E to 160.313E) and within each 1 hour period per UTC day,  the python 2.7 function histogram2d from module numpy has been used to create the strokecount variable. Histogram2d counts the number of scattered GPATS strokes occurring within each gridbox. GPATS data from 200803 - 201410 have been extracted from the Bureau of Meteorology adam_prd database. NOTE - Stroke count times are from minus one hour to the specified time in UTC.'

    dataset = netCDF4.Dataset(os.path.join(opath,oname), 'w', filenotes, format='NETCDF4')

    # Specify the dimensions in the netCDF file 
    londim = len(lon_data)
    latdim = len(lat_data)

    lon   = dataset.createDimension('lon', londim)
    lat   = dataset.createDimension('lat', latdim)
#     lev   = dataset.createDimension('lev', 1)
    time  = dataset.createDimension('time', 24)


    for dimname in dataset.dimensions.keys():
        dim = dataset.dimensions[dimname]

    # Now create the variables to assigned strings with defined dimensions.
    times       = dataset.createVariable('time','I', ('time',))
#     levels      = dataset.createVariable('level', 'I', ('lev',))
    latitudes = dataset.createVariable('latitude',np.float32, ('lat',))
    longitudes = dataset.createVariable('longitude',np.float32, ('lon',))
#     cloud_ground_neg = dataset.createVariable('cloud_ground_neg','I',('time','lev','lat','lon'))
#     cloud_ground_pos = dataset.createVariable('cloud_ground_pos','I',('time','lev','lat','lon'))
#     intra_cloud = dataset.createVariable('intra_cloud','I',('time','lev','lat','lon'))
#     total_lightning = dataset.createVariable('total_lightning','I',('time','lev','lat','lon'))
    cloud_ground_neg = dataset.createVariable('cloud_ground_neg','I',('time','lat','lon'))
    cloud_ground_pos = dataset.createVariable('cloud_ground_pos','I',('time','lat','lon'))
    intra_cloud = dataset.createVariable('intra_cloud','I',('time','lat','lon'))
    total_lightning = dataset.createVariable('total_lightning','I',('time','lat','lon'))


    # Global Attributes
    dataset.description = str(datestamp)
    dataset.history = 'Created '+str(dt.datetime.now().time())
    dataset.source = 'Dataset is stored on NCI Raijin in folder /g/data1/ua8/Lightning_Grid_EraInt/ . For details see http://climate-cms.unsw.wikispaces.net/Lightning+Stroke+Counts+on+ECMWF+Era+Interim+Grid          This file was created by Ross Bunn using gpatsgridded.py. Global Position and Tracking System or GPATS is a commercial lightning detection system, using ground-based Very Low Frequency sensors across Australia to accurately detect stroke times, and Time Difference of Arrival methods to calculate locations. On the ECMWF Era Interim Grid (lon res=0.703125deg, lat res= 0.701753deg), (or user defined grid) over an Australian domain (-45.263 to -9.473, 109.688E to 160.313E) and within each 1 hour period per UTC day,  the python 2.7 function histogram2d from module numpy has been used to create the strokecount variable. Histogram2d counts the number of scattered GPATS strokes occurring within each gridbox. GPATS data from 200803 - 201410 have been extracted from the Bureau of Meteorology adam_prd database. NOTE - Stroke count times are from minus one hour to the specified time in UTC.'

    # Variable Attributes
    longitudes.description = 'centre of the bin where number of strokes are counted'
    longitudes.standard_name = 'longitude'
    longitudes.long_name = 'longitude'
    longitudes.units = 'degrees east'
    longitudes.axis = 'X'
    latitudes.description = 'centre of the bin where number of strokes are counted'
    latitudes.standard_name = 'latitude'
    latitudes.long_name = 'latitude'
    latitudes.units = 'degrees north'
    latitudes.axis = 'Y'
#     levels.standard_name = 'air_pressure'
#     levels.long_name = 'pressure'
#     levels.units = 'hPa'
#     levels.positive = 'down'
#     levels.axis = 'Z'
    times.description = 'period is time minus 1 hour to time'
    times.standard_name = 'time'
    times.units = 'UTC'
    times.calendar = 'proleptic_gregorian'

    cloud_ground_neg.description = 'Number of cloud to ground strokes (negative current polarity) within Era Interim grid box (0.7deg) and within time period TIME-1hr to TIME.'
    cloud_ground_pos.description = 'Number of ground to cloud strokes (positive current polarity) within Era Interim grid box (0.7deg) and within time period TIME-1hr to TIME.'
    intra_cloud.description = 'Number of intra-cloud  strokes (zero current polarity) within Era Interim grid box (0.7deg) and within time period TIME-1hr to TIME.'
    total_lightning.description = 'Number of total strokes (negative, positive and zero current polarities) within Era Interim grid box (0.7deg) and within time period TIME-1hr to TIME.'

    #printing the variables to the netCDF files
    longitudes[:] = lon_data
    latitudes[:] = lat_data
#     levels[:]=1000
    times[:] = all_times
    cloud_ground_neg[:,:,:] = cg_daily
    cloud_ground_pos[:,:,:] = gc_daily
    intra_cloud[:,:,:] = ic_daily
    total_lightning[:,:,:] = tot_daily
    
    
    dataset.close() #close the new netCDF file
    
    print "netCDF populated with "+str(datestamp)+" data .... ",os.path.join(opath,oname)


if __name__ == "__main__":
    main(yr, test, plot)   

