""" Script creates a new netCDF using existing Era Interim or user defined grid, time and data."""

import numpy as np
from netCDF4 import Dataset
import time
import datetime
import netCDF4
import sys
import os
from nco import Nco
nco = Nco()

def create(opath,oname,datestamp,lon_data,lat_data,ipath="/Users/bunnr/1_DATA/eraint_reanalysis/nc/upper",iname="T_RH_GH_20080101_00.nc"):
    # Create the grid to bin the number of stokes. 
    # Grid domain is over tropics, Tasma, India Oceans. Over ACCESS A domain -55 to 4.73, 95 to 169.69
    ifile = os.path.join(ipath,iname)
    gridlon = np.array(nco.ncks(input=ifile, returnArray="lon")) #extract Era Int Lat points over Australia
    gridlon = gridlon.astype(np.float)
    gridlat = np.array(nco.ncks(input=ifile, returnArray="lat")) #extract Era Int Lon points over Australia
    gridlat = gridlat.astype(np.float)

    # datechar = netCDF4.stringtochar(date)
    # print datechar

    filenotes = "Created "+str(datetime.datetime.now().time())+\
                "This file was created by Ross Bunn using gpatsgridded.py." + \
                " Global Position and Tracking System or GPATS is a commercial "+\
                "lightning detection system, using ground-based Very Low Frequency "+\
                "sensors across Australia to accurately detect stroke times, and Time "+\
                "Difference of Arrival methods to calculate locations. On the ECMWF Era "+\
                "Interim Grid (lon res=0.703125deg, lat res= 0.701753deg), (or user defined grid)"+\
                " over an Australian domain (-45.263 to -9.473, 109.688E to 160.313E) and within each "+\
                "1 hour period per UTC day,  the python 2.7 function histogram2d from module numpy has "+\
                "been used to create the strokecount variable. Histogram2d counts the number of scattered "+\
                "GPATS strokes occurring within each gridbox. GPATS data from 200803 - 201410 have been extracted "+\
                "from the Bureau of Meteorology adam_prd database. NOTE - Stroke count times are from minus one hour "+\
                "to the specified time in UTC.'

    dataset = Dataset(os.path.join(opath,oname), 'w', filenotes, format='NETCDF4')

    # Specify the dimensions in the netCDF file 
    londim = len(lon_data)
    latdim = len(lat_data)

    lon   = dataset.createDimension('lon', londim)
    lat   = dataset.createDimension('lat', latdim)
    lev   = dataset.createDimension('lev', 1)
    time  = dataset.createDimension('time', 24)


    for dimname in dataset.dimensions.keys():
        dim = dataset.dimensions[dimname]

    # Now create the variables to assigned strings with defined dimensions.
    longitude = dataset.createVariable('longitude',np.float32, ('lon',))
    latitude  = dataset.createVariable('latitude',np.float32, ('lat',))
    level      = dataset.createVariable('level', 'I', ('lev',))
    time       = dataset.createVariable('time','I', ('time',))
    cloud_ground = dataset.createVariable('cloud_ground',np.float32,('lat','lon','time'))
    ground_cloud = dataset.createVariable('ground_cloud',np.float32,('lat','lon','time'))
    intra_cloud = dataset.createVariable('intra_cloud',np.float32,('lat','lon','time'))
    total_lightning = dataset.createVariable('total_lightning',np.float32,('lat','lon','time'))


    # Global Attributes
    dataset.description = str(datestamp)
    dataset.source = 'Dataset is stored on NCI Raijin in folder /g/data1/ua8/Lightning_Grid_EraInt/ . For details see http://climate-cms.unsw.wikispaces.net/Lightning+Stroke+Counts+on+ECMWF+Era+Interim+Grid '

    # Variable Attributes
    longitude.description = 'centre of the bin where number of strokes are counted'
    longitude.standard_name = 'longitude'
    longitude.long_name = 'longitude'
    longitude.units = 'degrees east'
    longitude.axis = 'X'
    latitude.description = 'centre of the bin where number of strokes are counted'
    latitude.standard_name = 'latitude'
    latitude.long_name = 'latitude'
    latitude.units = 'degrees north'
    latitude.axis = 'Y'
    level.standard_name = 'air_pressure'
    level.long_name = 'pressure'
    level.units = 'hPa'
    level.positive = 'down'
    level.axis = 'Z'
    time.description = 'period is time minus 1 hour to time'
    time.standard_name = 'time'
    time.units = 'UTC'
    time.calendar = 'proleptic_gregorian'

    cloud_ground.description = 'Number of cloud to ground strokes (negative current polarity) within Era Interim grid box (0.7deg) and within time period TIME-1hr to TIME.'
    ground_cloud.description = 'Number of ground to cloud strokes (positive current polarity) within Era Interim grid box (0.7deg) and within time period TIME-1hr to TIME.'
    intra_cloud.description = 'Number of intra-cloud  strokes (zero current polarity) within Era Interim grid box (0.7deg) and within time period TIME-1hr to TIME.'
    total_lightning.description = 'Number of total strokes (negative, positive and zero current polarities) within Era Interim grid box (0.7deg) and within time period TIME-1hr to TIME.'

    #printing the variables to the netCDF files
    longitude[:] = binlon
    latitude[:] = binlat
    level[:]=1000

    dataset.close() #close the new netCDF file
    
    print "netCDF created.... ", os.path.join(opath,oname)

def populate(opath,oname,this_time,cloud_ground,ground_cloud,intra_cloud,total_lightning):
    
    dataset = Dataset(os.path.join(opath,oname),'w', format='NETCDF4')

    time[:] = this_time
    cloud_ground[:,:] = cloud_ground
    ground_cloud[:,:] = ground_cloud
    intra_cloud[:,:] = intra_cloud
    total_lightning[:,:] = total_lightning

    dataset.close() #close the netCDF file

    print "netCDF populated with "+str(this_time)+" data .... ",os.path.join(opath,oname)

