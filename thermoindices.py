"""Module contains functions which create a new dataframe with read-in datetime stamp sounding times
    at aerodromes where gpats lightning occurred, and populates with thermodynamic indicies based in sounding data.
    
    create(inpath,fname ):
        This function creates a dataframe from dfthermo, either an empty dataframe with column headings, 
        or reads in an existing csv file with datetime stamps and station names.
        return dfthermo
    
    populate(pres,temp,dwpt,hght,dfthermo,kk):
        This function takes existing dataframe of sounding data at a site, date and time, and populates 
        a dataframe dfthermo with thermodynamic indicies.
        return dfthermo
       
      """
import pandas as pd
import numpy as np 
import sys
from skewt import SkewT
from skewt import thermodynamics as thrm
import sharppy.sharptab as tab
import sharppy.sharptab.profile as tabprof
import os
    
def create(inpath,fname,dtstampname ):
    """This function creates a dataframe from dfthermo, either an empty dataframe with column headings, 
       or reads in an existing csv file with datetime stamps and station names. """
    if os.path.isfile(os.path.join(inpath,fname)) == True:
        #  Reading in dates/times per capital city where known gpats activity was recorded
        dfthermo = pd.read_csv(os.path.join(inpath,fname), 
                               converters={'BoM Station ID': lambda x: str(x).zfill(6)}, 
                               parse_dates=[dtstampname], index_col = False)
    
#         dfthermo = dfthermo.sort(["Station Name", "datestamp"])  # sort by name and date
        dfthermo = dfthermo.sort([dtstampname])  # sort by date
        index = [dfi for dfi in xrange(len(dfthermo))]
        columns = np.char.array(["LevArr Len",
                                 "Theta E Surface (K)","Theta E Max upper (K)",
                                 "Lev Theta E Max upper (hPa)","CAPE Surface (J/kg)","CIN Surface (J/kg)",
                                 "Lev Surface (hPa)","LCL Surface (hPa)", "LFC Surface (hPa)", "EL Surface (hPa)",
                                 "T Surface (C)","TD Surface (C)","MUCAPE (J/kg)","MUCIN (J/kg)","Lev MUCAPE (hPa)",
                                 "LCL MUCAPE (hPa)", "LFC MUCAPE (hPa)", "EL MUCAPE (hPa)",
                                 "T MUCAPE (C)","TD MUCAPE (C)", 
                                 "Effective Inflow Bottom Level (hPa)", "Effective Inflow Top Level (hPa)",
                                 "Effective Inflow Layer Bottom Height (m AGL)", 
                                 "Effective Inflow Layer Top Height (m AGL)"])
#         ,"Weather Type","Weather Reported"

        df = pd.DataFrame(index = index, columns=columns)
        dfthermo = dfthermo.join(df)
        del df

    else:
        #  create empty dataframe for later filling
        columns = np.char.array(["datestamp","Station Name","BoM Station ID","WMO ID",
                                 "LevArr Len",
                                 "Theta E Surface (K)","Theta E Max upper (K)",
                                 "Lev Theta E Max upper (hPa)","CAPE Surface (J/kg)","CIN Surface (J/kg)",
                                 "Lev Surface (hPa)","LCL Surface (hPa)", "LFC Surface (hPa)", "EL Surface (hPa)",
                                 "T Surface (C)","TD Surface (C)","MUCAPE (J/kg)","MUCIN (J/kg)","Lev MUCAPE (hPa)",
                                 "LCL MUCAPE (hPa)", "LFC MUCAPE (hPa)", "EL MUCAPE (hPa)",
                                 "T MUCAPE (C)","TD MUCAPE (C)", 
                                 "Effective Inflow Bottom Level (hPa)", "Effective Inflow Top Level (hPa)",
                                 "Effective Inflow Layer Bottom Height (m AGL)", 
                                 "Effective Inflow Layer Top Height (m AGL)"])
        dfthermo = pd.DataFrame(columns=columns)

    return dfthermo

def populatesharppy(pres,temp,dwpt,hght,dfthermo,kk ):    
    """This function takes existing dataframe of sounding data at a site, date and time, and populates 
       a dataframe dfthermo with thermodynamic indicies calculated using SHARPpy. SHARPpy is able to calculate Effective Inflow layers, 
       and uses the Virtual Temperature correction in all CAP and CIN calculations. 
    """
    assert not isinstance(pres, basestring), "pres is not an array"
    assert not isinstance(temp, basestring), "temp is not an array"
    assert not isinstance(dwpt, basestring), "dwpt is not an array"
    assert not isinstance(hght, basestring), "hght is not an array"

    a = np.array(np.isnan(pres), dtype=bool)
    b = np.array(np.isnan(temp), dtype=bool)
    c = np.array(np.isnan(dwpt), dtype=bool)
    d = np.array(np.isnan(hght), dtype=bool)
    if (a.any())|(b.any())|(c.any())|(d.any())==True:
        print "Arrays in populatesharppy have NaN... filling NaN with -9999"
        pres = pd.DataFrame(pres)
        pres = pres.where((pd.notnull(pres)), int(-9999))   # replace NaN with -9999
        pres = (pres[pres.columns[0]]).tolist()
        temp = pd.DataFrame(temp)
        temp = temp.where((pd.notnull(temp)), int(-9999))   # replace NaN with -9999
        temp = (temp[temp.columns[0]]).tolist()
        dwpt = pd.DataFrame(dwpt)
        dwpt = dwpt.where((pd.notnull(dwpt)), int(-9999))   # replace NaN with -9999
        dwpt = (dwpt[dwpt.columns[0]]).tolist()
        hght = pd.DataFrame(hght)
        hght = hght.where((pd.notnull(hght)), int(-9999))   # replace NaN with -9999
        hght = (hght[hght.columns[0]]).tolist()
        a = np.array(np.isnan(pres), dtype=bool)
        b = np.array(np.isnan(temp), dtype=bool)
        c = np.array(np.isnan(dwpt), dtype=bool)
        d = np.array(np.isnan(hght), dtype=bool)
        if (a.any())==True:
            print "NaN still exists in pres...EXITING!"
            print pres
            sys.exit() 
        if (b.any())==True:
            print "NaN still exists in temp...EXITING!"
            print temp
            sys.exit() 
        if (c.any())==True:
            print "NaN still exists in dwpt...EXITING!"
            print dwpt
            sys.exit() 
        if (d.any())==True:
            print "NaN still exists in hght...EXITING!"
            print hght
            sys.exit() 
                               
    prof = tabprof.create_profile(profile='default', pres=pres, hght=hght, tmpc=temp, dwpc=dwpt, \
                                  wdir = np.empty([len(pres)]), wspd = np.empty([len(pres)]), \
                                  missing=-9999, strictQC=False)
                                  
    sfcpcl = tab.params.parcelx( prof, flag=1 ) # Surface Parcel
    mupcl = tab.params.parcelx( prof, flag=3 ) # Most-Unstable Parcel
#     mlpcl = tab.params.parcelx( prof, flag=4 ) # 100 mb Mean Layer Parcel
    eff_inflow = tab.params.effective_inflow_layer(prof)
    ebot_hght = tab.interp.to_agl(prof, tab.interp.hght(prof, eff_inflow[0]))
    etop_hght = tab.interp.to_agl(prof, tab.interp.hght(prof, eff_inflow[1]))

    # now replace -9999 values with np.nan for parameter calculations
    pres = pd.DataFrame(pres)
    pres = pres.applymap(lambda x: np.nan if x==-9999 else x)   # replace -9999 with NaN
    pres = (pres[pres.columns[0]]).tolist()
    temp = pd.DataFrame(temp)
    temp = temp.applymap(lambda x: np.nan if x==-9999 else x)   # replace -9999 with NaN
    temp = (temp[temp.columns[0]]).tolist()
    dwpt = pd.DataFrame(dwpt)
    dwpt = dwpt.applymap(lambda x: np.nan if x==-9999 else x)   # replace -9999 with NaN
    dwpt = (dwpt[dwpt.columns[0]]).tolist()
    hght = pd.DataFrame(hght)
    hght = hght.applymap(lambda x: np.nan if x==-9999 else x)   # replace -9999 with NaN
    hght = (hght[hght.columns[0]]).tolist()

    # calculate thetae at all levels - for maxthtae level
    thetae = np.zeros(len(pres),dtype=float) 
    for j in range(len(pres)):
        thetae[j] = tab.thermo.thetae(pres[j],temp[j],dwpt[j]) # full thtae profile
    # find max THETA E value and level were it occurs
    # create separate dataframes
    dftmp1 = pd.DataFrame({"pres": pres})
    dftmp2 = pd.DataFrame({"thetae": thetae})
    # join separate dfs together
    dftmp3 = pd.concat([dftmp1, dftmp2], axis=1)
    dftmp4 = dftmp3[dftmp3.pres>=600.0]            # slice df for pres <=600hPa
    thetae=dftmp4.thetae.dropna()                  # restricted theta, only real non missing values
    del dftmp1,dftmp2,dftmp3,dftmp4
 #  thetae = thetae[~np.isnan(thetae)]             # only real non missing values
    I=np.argmax(thetae)                            # index where maximum theta E value occurs
    dfthermo.set_value(kk,"Theta E Max upper (K)",float(thetae[I]))
    dfthermo.set_value(kk,"Lev Theta E Max upper (hPa)",float(pres[I]))
    del thetae
    
    sfcthte = tab.thermo.thetae(sfcpcl.pres, sfcpcl.tmpc, sfcpcl.dwpc )
    muthte = tab.thermo.thetae(mupcl.pres, mupcl.tmpc, mupcl.dwpc )

    # Populate Surface Variables to df
    dfthermo.set_value(kk,"LevArr Len",int(len(pres)))
    dfthermo.set_value(kk,"Lev Surface (hPa)",float(sfcpcl.pres))
    dfthermo.set_value(kk,"T Surface (C)",float(sfcpcl.tmpc))
    dfthermo.set_value(kk,"TD Surface (C)",float(sfcpcl.dwpc))
    dfthermo.set_value(kk,"CAPE Surface (J/kg)",float(sfcpcl.bplus))
    dfthermo.set_value(kk,"CIN Surface (J/kg)",float(sfcpcl.bminus))
    dfthermo.set_value(kk,"LCL Surface (hPa)",float(sfcpcl.lclpres))
    dfthermo.set_value(kk,"LFC Surface (hPa)",float(sfcpcl.lfcpres))
    dfthermo.set_value(kk,"EL Surface (hPa)",float(sfcpcl.elpres))
    dfthermo.set_value(kk,"Theta E Surface (K)",float(sfcthte))

    # MUCAPE, MUCIN to df
    dfthermo.set_value(kk,"Lev MUCAPE (hPa)",float(mupcl.pres))
    dfthermo.set_value(kk,"T MUCAPE (C)",float(mupcl.tmpc))
    dfthermo.set_value(kk,"TD MUCAPE (C)",float(mupcl.dwpc))
    dfthermo.set_value(kk,"MUCAPE (J/kg)",float(mupcl.bplus))
    dfthermo.set_value(kk,"MUCIN (J/kg)",float(mupcl.bminus))
    dfthermo.set_value(kk,"LCL MUCAPE (hPa)",float(mupcl.lclpres))
    dfthermo.set_value(kk,"LFC MUCAPE (hPa)",float(mupcl.lfcpres))
    dfthermo.set_value(kk,"EL MUCAPE (hPa)",float(mupcl.elpres))

    # Effective Inflow to df
    dfthermo.set_value(kk,"Effective Inflow Bottom Level (hPa)",float(eff_inflow[0]))
    dfthermo.set_value(kk,"Effective Inflow Top Level (hPa)",float(eff_inflow[1]))
    dfthermo.set_value(kk,"Effective Inflow Layer Bottom Height (m AGL)",float(ebot_hght))
    dfthermo.set_value(kk,"Effective Inflow Layer Top Height (m AGL)",float(etop_hght))

    return dfthermo
    
    
def populate(pres,temp,dwpt,hght,dfthermo,kk ):
    """This function takes existing dataframe of sounding data at a site, date and time, and populates 
       a dataframe dfthermo with thermodynamic indicies. """
    assert not isinstance(pres, basestring), "pres is not an array"
    assert not isinstance(temp, basestring), "temp is not an array"
    assert not isinstance(dwpt, basestring), "dwpt is not an array"
#   assert dtype(kk) == int, "index kk is not an integer"

#   mydata=dict(zip(('hght','pres','temp','dwpt','sknt','drct'),(a,b,c,d,e,f)))

    mydata=dict(zip(('pres','temp','dwpt','hght'),(pres,temp,dwpt,hght))) 
     
    S=SkewT.Sounding(soundingdata=mydata);                         # read in cleaned sounding data

    temp = S.soundingdata['temp'];
    dwpt = S.soundingdata['dwpt'];
    pres = S.soundingdata['pres'];
    hght = S.soundingdata['hght'];

#     print "temp in skewt", temp
#     print "dwpt in skewt", dwpt
#     print "pres in skewt", pres
#     print "hght in skewt", hght

    # calculate theta and thetae at all levels
    thetae = np.zeros(len(pres),dtype=float) 
    theta = np.zeros(len(pres),dtype=float)
    for j in range(len(pres)):
        e= thrm.VaporPressure(float(dwpt[j])); 
        thetae[j] = thrm.ThetaE(float(temp[j])+ 273.15, float(pres[j])*100, e);
        theta[j] = thrm.Theta(float(temp[j])+ 273.15, float(pres[j])*100);

    # *********************************************
    # get surface surface thermo variables
    # *********************************************
    # define parcel, sb=suface, mu=most unstable, ml=mixed layer
    # returns [pres[sb], T[sb], TD[sb]]
    parcel=S.get_parcel("sb")
#                 print "SB PARCEL: ", parcel
    lcl,lfc,el,cape,cin=S.get_cape(*parcel)        # get parcel cape, cin

#     try:                      
#         lcl,lfc,el,cape,cin=S.get_cape(*parcel);         # get parcel cape, cin
#     except IndexError:
#         "tried to lift parcel...exiting..."
#         sys.exit()
#     print "inside thermoindicies.py"
#     print "lcl,lfc,el,cape,cin", lcl,lfc,el,cape,cin
#     print parcel


    # Populate Surface Variables to df
    dfthermo.set_value(kk,"LevArr Len",int(len(pres)))
    dfthermo.set_value(kk,"Lev Surface (hPa)",float(parcel[0]))
    dfthermo.set_value(kk,"T Surface (C)",float(parcel[1]))
    dfthermo.set_value(kk,"TD Surface (C)",float(parcel[2]))
    dfthermo.set_value(kk,"CAPE Surface (J)",float(cape))
    dfthermo.set_value(kk,"CIN Surface (J)",float(cin))
    dfthermo.set_value(kk,"LCL Surface (hPa)",float(lcl))
    dfthermo.set_value(kk,"LFC Surface (hPa)",float(lfc))
    dfthermo.set_value(kk,"EL Surface (hPa)",float(el))
    dfthermo.set_value(kk,"Theta E Surface (K)",float(thetae[0]))


    # *********************************************
    # get upper surface thermo variables
    # ********************************************* 
    # MUCAPE function finds most unstable in lowest 300hPa of sounding
    #
    # define parcel, sb=suface, mu=most unstable, ml=mixed layer
    # returns [pres[mu], T[mu], TD[mu]]
#                 parcel=S.get_parcel("mu")[:3]   
    parcel=S.get_parcel("mu");                     

    lcl,lfc,el,cape,cin=S.get_cape(*parcel);         # get parcel cape, cin
#     print "inside thermoindicies.py"
#     print "lcl,lfc,el,cape,cin", lcl,lfc,el,cape,cin
#     print parcel

    # MUCAPE, MUCIN to df
    dfthermo.set_value(kk,"Lev MUCAPE (hPa)",float(parcel[0]))
    dfthermo.set_value(kk,"T MUCAPE (C)",float(parcel[1]))
    dfthermo.set_value(kk,"TD MUCAPE (C)",float(parcel[2]))
    dfthermo.set_value(kk,"MUCAPE (J)",float(cape))
    dfthermo.set_value(kk,"MUCIN (J)",float(cin))
    dfthermo.set_value(kk,"LCL MUCAPE (hPa)",float(lcl))
    dfthermo.set_value(kk,"LFC MUCAPE (hPa)",float(lfc))
    dfthermo.set_value(kk,"EL MUCAPE (hPa)",float(el))

    # find max THETA E value and level were it occurs
    # create separate dataframes
    dftmp1 = pd.DataFrame({"pres": pres})
    dftmp2 = pd.DataFrame({"thetae": thetae})
    # join separate dfs together
    dftmp3 = pd.concat([dftmp1, dftmp2], axis=1)
    dftmp4 = dftmp3[dftmp3.pres>=600.0]            # slice df for pres <=600hPa
    thetae=dftmp4.thetae.dropna()                  # restricted theta, only real non missing values
    del dftmp1,dftmp2,dftmp3,dftmp4
 #  thetae = thetae[~np.isnan(thetae)]             # only real non missing values
    I=np.argmax(thetae)                            # index where maximum theta E value occurs
    dfthermo.set_value(kk,"Theta E Max upper (K)",float(thetae[I]))
    dfthermo.set_value(kk,"Lev Theta E Max upper (hPa)",float(pres[I]))

    del theta, thetae

    return dfthermo