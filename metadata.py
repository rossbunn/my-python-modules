import os, sys, argparse
import datetime
from git import Repo

def main(path = os.getcwd(), fname = "newmetafile", message=None):
    """creates a <filename.met> metadata file which is associated with the image
    An example using this function would be:
        # save the fig and create a .met file
        path = "/Users/bunnr/python/workshop/plots"
        imgname = "hist_ts_station_daynight_20082015"
        savefig(os.path.join(path,imgname+".png"), dpi=500) # save the figure
        metadata.main(path,imgname)
    """ 
    metafile = os.path.join(path,fname+".met")
#     os.system("echo create_history() >> " + metafile)
    print metafile
    f = open(metafile, 'w')
    if message!=None:
        f.write(message +"\n") 
        f.write(create_history()) 
    else:
        f.write(create_history()) 
    f.close()

def create_history(author="Ross Bunn"):
    """Create the new entry for the global history file attribute. Give the author name if not Ross Bunn"""
    author = author
    time_stamp = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
    exe = sys.executable
    args = " ".join(sys.argv)
    git_hash = Repo(os.getcwd()).heads[0].commit

    return """Created by %s - %s: %s %s (Git hash: %s)""" %(author, time_stamp, exe, args, str(git_hash)[0:7])
  

main()